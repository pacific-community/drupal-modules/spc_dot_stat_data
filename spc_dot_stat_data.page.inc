<?php

/**
 * @file
 * Contains dot_stat_data.page.inc.
 *
 * Page callback for SPC .Stat Data entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for SPC .Stat Data templates.
 *
 * Default template: dot_stat_data.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_spc_dot_stat_data(array &$variables) {
  // Fetch DotStatData Entity Object.
  $dot_stat_data = $variables['elements']['#dot_stat_data'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
