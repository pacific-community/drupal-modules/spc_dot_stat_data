# SPC Dot Stat

Drupal 8/9 module to harvest data from PDH.stat and display data, tables or charts using custom blocks.

## Installing

### Download using composer

Setup Git repository
```
composer config repositories.spc_dot_stat_data vcs \
 git@gitlab.com:pacific-community/drupal-modules/spc_dot_stat_data.git
```

Download module
```
composer require pacific_community/spc_dot_stat_data:^1.0
```

### Enable module

Through admin GUI or using drush:
```
drush pm-enable spc_dot_stat_data
```

### Setup

Configure options on module's configuration page at _admin/config/development/dot_stat_data_.

* __Bootstrap__: if your theme doesn't include Bootstrap CSS and JS, choose one library to include.  
  (recommended option is 4.5.1 from CDN)


## Upgrade notes

### 1.3.1 Maps options

Need to define plotOptions.map.dataLabels.format in Highcharts configuration:
```
{
  plotOptions: {
    map: {
      dataLabels: {
       format: '{point.code2}',
        allowOverlap: true
      }
    }
  }
}
```

### 1.6.0 .stat dataflows to replace .stat links module

Make sure you disable spc_dot_stat_links module before upgrading

* Remove .stat links blocks if any
* Disable spc_dot_stat_links
* Enable spc_dot_stat_dataflows
* Configure .stat dataflows (/admin/config/development/dot_stat_dataflows)
* Synchronize .stat categories
* Synchronize .stat dataflows
* Replace links blocks with new ones from .stat dataflows


## Version History
* 1.7.4 : Fix to handle to new SDMX/JSON syntax 
  * Fix: add missing files for lollipop and dumbbell
* 1.7.3 : Update highcharts maps to v10.1
  * Fix error with maps
  * Use of GeoJSON sources for maps
* 1.7.2 : Update highcharts to v10.1
  * Adapted drilldown code with new breadcrumbs feature
* 1.7.1 : Sort dimensions in .stat response
  * Added sorting of dimensions in .stat response parsing
  * Added JSON viewer for a better user experience in drupal backoffice
  * Added french convention for numbers in Key Stat
* 1.7.0 : Add compatibility to Drupal 9
  * Replace deprecated 'context' key for 'context_definitions' in Plugins definition (https://www.drupal.org/node/3016699)
  * Fix parsing of Highcharts options
* 1.6.9 : Javascript chart config parser fix
  * Updated REGEX to better ignore functions from chart configuration
* 1.6.8 : Permission fix
  * Fixed permissions to manage .stat data entities
* 1.6.7 : Grouping multi columns chart
  * Calculate percentage (experiment for Vaccination chart)
  * Reorder chart by values (experiment for Vaccination chart)
* 1.6.6 : Data table block order options
  * Order by column label : { "order": "column 1, column 2, ..." }
  * Order automatically : { "order": "asc|desc" }
* 1.6.5 : Fixing missing values bug
  * Time series now show gaps when value is missing
* 1.6.4 : cleaner dataflows module removal
  * Cleans storage config
* 1.6.3 : .stat dataflows block layout options
  * Expanded or Collapsed or Link to entity only
  * Full or limited list of indicators
* 1.6.2 : Population pyramid x-axis fix
  * Showing age range as label
* 1.6.1 : .stat links submodule removed
  * MAKE SURE YOU DISABLE spc_dot_stat_links before upgrading
  * .stat dataflows blocks improvements (more display modes available)
* 1.6.0 : .stat dataflows sub module
  * .stat links module rollback (soon to be deprecated)
  * .stat dataflows use content type instead of custom entities
* 1.5.1 : .stat links module update fix
  * Create new data entity table on module update
  * Change config key to match module's name
* 1.5.0 : Links to .stat dataflows by drupal entity
  * Load up categories and dataflows in drupal entities
  * Display links using new "Dataflow" block
* 1.4.0 : Country label improvements and multi column fix
  * Fixed Multi column missing values
  * Added Allow use of 2-letter codes, short names or full name in map
  * New dataflow links entities in links module (alpha version)
* 1.3.0 : Javascript code revised, additional features and fixes
  * Javascript code revised to support multiple series
  * New grouped columns chart template
  * Map module specific configuration implemented in PHP (and extended in JS)
  * Data parser filters fix
  * Better logging on API request failure
* 1.2.9 : Parser optimisation
  * Adding filters in parser configuration
  * New Pacific maps with land
* 1.2.8 : Caching improvement
  * Added caching tags to blocks
  * Invalidates tags on dot_stat_entity update (node dependant)
* 1.2.7 : Highmaps block custom configuration support
  * Highmaps javascript custom object 
  * Highmaps load json configuration from block
* 1.2.6 : Better field support
  * Field/value support in title {{field}} added to charts
  * Filter {{field}} to show last (max) value only
* 1.2.5 : Missing JS files updates
  * Fix missing modules updates to version 8.2.2
  * Covid map cases within 42 days fix
* 1.2.4 : Charts and Maps mix fix
  * Highcharts library updated to version 8.2.2 (2020-10-22)
* 1.2.3 : Fixes and improvements + maps module (beta)
  * Ajax calls fix (live data update)
  * Field/value support in title {{field}}
  * Maps visualisation submodule (beta)
* 1.2.2 : Code cleaning and improvements
  * Using Drupal JS API (rather than pure Jquery)
  * Data entities background updates by cron queue workers
* 1.2.1 : Global configuration
  * New global configuration forms for each chart template
* 1.2.0 : Code cleaning and Population pyramid race
  * Javascript code revised (separate lib/class depending on chart template)
  * New chart: Population pyramid race
  * Removed Chart Summary block/template
* 1.1.9 : IE11 compatibility fix
* 1.1.8 : Dynamic data loading
  * Load data (ajax) and update chart (experimental)
  * CSS fix for Population pyramid selector
* 1.1.7 : No visualisation option
  * No chart template selected: no visualisation icon and text
  * No data entity selected: no data available (no link)
* 1.1.6 : Responsive charts and dynamic functions
  * Time series: showing legend at the bottom on small screens
  * New JS method to update data and configuration live
* 1.1.5 : Improvements for tables
  * New Yes/No icons
  * Fix tale headers alignment
* 1.1.4 : Chart improvements
  * Timeseries: allow forcing line colors and codes
  * Drilldown: change subtitle on drill down
  * Data Tables: refactored and now supports data series (multi columns)
* 1.1.3 : SDGs customizations
  * Summary chart can include number of inidicators (use XXX in title or tooltips)
  * Pie Summary include only specific countries (CK, FJ, FM, KI, PF, MH, NC, NR, NU, PG, PW, SB, TO, TV, VU, WS)
* 1.1.2 : Improvements
  * New chart: Lollipop charts
* 1.1.1 : New charts and fixes
  * New chart : Pie chart Summary to aggregate countries
  * New block : Chart Overview to summarize SDG stats per country
  * Settings namespace fix: renamed to spc_dot_stat_data.settings
* 1.1.0 : Code structure revamp and improvements
  * Code revamped to add better template support per chart type
  * Adding series definition to parsed JSON saved in entities
  * Pre-parsing .stat JSON to preview when editing entities
  * Prettified JSON when viewing data entitiees
  * New chart type: Pie chart
  * New chart type: Time series (line)
  * Replaced Drilldown block type with chart template
  * Better icons for key stat blocks
  * Fix: all blocks now include Javascript bootstrap library (according to global settings)
* 1.0.3 : Order sub datasets by ORDER annotation
* 1.0.2 : Fixes and improvements
  * Name field length increased to 128 (DB update)
  * Check API request and parse JSON for dimensions and attributes
* 1.0.1 : Drupal 9 compatibility fix
* 1.0.0 : Initial Release

## TODO
* List and load extra chart templates from theme
* List and load extra icons from theme
