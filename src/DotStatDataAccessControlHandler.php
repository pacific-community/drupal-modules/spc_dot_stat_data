<?php

namespace Drupal\spc_dot_stat_data;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the SPC .Stat Data entity.
 *
 * @see \Drupal\spc_dot_stat_data\Entity\DotStatData.
 */
class DotStatDataAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\spc_dot_stat_data\Entity\DotStatDataInterface $entity */
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view .stat data entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit .stat data entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete .stat data entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add .stat data entities');
  }

}
