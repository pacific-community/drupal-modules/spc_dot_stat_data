<?php

namespace Drupal\spc_dot_stat_data\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting SPC .Stat Data entities.
 *
 * @ingroup spc_dot_stat_data
 */
class DotStatDataDeleteForm extends ContentEntityDeleteForm {


}
