<?php

namespace Drupal\spc_dot_stat_data\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;


/**
 * Form controller for SPC .Stat Data edit forms.
 *
 * @ingroup spc_dot_stat_data
 */
class DotStatDataForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\spc_dot_stat_data\Entity\DotStatData */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    parent::submitForm($form, $form_state);
    
    // Query dot stat and parse results
    $service = \Drupal::service('spc_dot_stat_data.request');
    
    // form element values
    $values = $form_state->getUserInput();
    
    $qry = $values['data_query'][0]['value'];
    $parser = trim($values['data_parser'][0]['value']);
    
    $json = $service->getParsedResults($qry, $parser);
    
    $this->entity->setDataJson($json);
    
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    $messenger = \Drupal::messenger();

    switch ($status) {
      case SAVED_NEW:
        $messenger->addMessage($this->t('Created the %label data entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $messenger->addMessage($this->t('Saved the %label data entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.dot_stat_data.canonical', ['dot_stat_data' => $entity->id()]);
  }

}
