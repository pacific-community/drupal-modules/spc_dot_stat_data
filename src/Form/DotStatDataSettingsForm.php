<?php

namespace Drupal\spc_dot_stat_data\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DotStatDataSettingsForm.
 *
 * @ingroup spc_dot_stat_data
 */
class DotStatDataSettingsForm extends ConfigFormBase {

  const SETTINGS = 'spc_dot_stat_data.settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'spc_dot_stat_data_settings';
  }

  /**
   * Defines the settings form for SPC .Stat Data entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    // Default settings.
    $config = $this->config(static::SETTINGS);

    // Force update
    $form['bootstrap'] = [ 
      '#type' => 'select',
      '#title' => $this->t('Include bootstrap CSS and JS files'),
      '#options' => [
        '' => $this->t('None'),  
        'bootstrap3' => $this->t('Bootstrap 3.4.1 (local)'),
        'bootstrap3-cdn' => $this->t('Bootstrap 3.4.1 (CDN)'),
        'bootstrap4-cdn' => $this->t('Bootstrap 4.5.1 (CDN)')
      ],
      '#default_value' => $config->get('bootstrap'),
      '#description' => 'Select Bootstrap library to include<br />(use only if not already included in your theme)'
    ];
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('bootstrap', $form_state->getValue('bootstrap'))
      ->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS
    ];
  }

}
