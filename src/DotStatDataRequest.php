<?php

namespace Drupal\spc_dot_stat_data;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Query Dot Stat data and structure definitions
 */
class DotStatDataRequest {

  /**
   * The http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $client;
  
  /**
   * DotStatRequest constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The guzzle http client.
   */
  public function __construct(ClientInterface $http_client) {
    $this->client = $http_client;
  }

  /**
   * get JSON back from .stat API query
   */
  public function queryXml($url) {
    $client = \Drupal::httpClient();

    try {
      $response = $client->get($url, [
        'verify' => false // -TODO- disable in production
      ]);
      // Expected result.
      // getBody() returns an instance of Psr\Http\Message\StreamInterface.
      // @see http://docs.guzzlephp.org/en/latest/psr7.html#body
      $data = $response->getBody();
      
      $xmlstring = $data->getContents();

      return $xmlstring;
    }
    catch (RequestException $e) {
      watchdog_exception('spc_dot_stat_data', $e);
    }

    return false;
    
  }
  
  /**
   * get JSON back from .stat API query
   */
  public function queryRaw($url) {
    $client = \Drupal::httpClient();

    try {
      $url .= (parse_url($url, PHP_URL_QUERY) ? '&' : '?') . 'format=jsondata';
      $response = $client->get($url);
      // Expected result.
      // getBody() returns an instance of Psr\Http\Message\StreamInterface.
      // @see http://docs.guzzlephp.org/en/latest/psr7.html#body
      $data = $response->getBody();
      
      $jstring = $data->getContents();

      return $jstring;
      //return $data->getContents();
    }
    catch (RequestException $e) {
      \Drupal::logger('spc_dot_stat_data')->error($e->getMessage());
      return null;
    }
    
  }

  /**
   * Key/Value parser helper
   * @see parseRow for more information
   */
  protected function parseKeyValue($param, $rowkey, $values, $dims, $attrs) {
    $result = null;
    if ($param == 'value') {
      $param = 'val[0]'; // alias
    }
    if (preg_match('/^(dim|val|attr)\[(\d+)\](\.(.+))?$/', $param, $red)) {
      switch ($red[1]) {
        case 'dim':
          // gets data from dimensions structure, indexed from observations' key at certain position
          // dim[position].property eg. dim[3].name
          $i = $red[2];
          $n = explode(':',$rowkey)[$i];
          $p = $red[4];
          $result = isset($n, $p)?$dims[$i]['values'][$n][$p]:null;
          break;
        case 'attr':
          // look for attributes
          // attr[position].property eg. attr[4].name
          $i = $red[2];
          $n = $values[$i];
          $p = $red[4];
          $result = isset($n, $p)?$attrs[$i-1]['values'][$n][$p]:null;
          break;
        case 'val':
          // gets data from array of observations's value
          // val[position] eg. val[0]
          $i = $red[2];
          $result = $values[$i];
          break;
      }
    }
    return $result;
  }
  
  
  /**
   * Internal row parser
   *
   * This is called by getParsedResults method, for each row found in results.
   *
   * Each row is parsed following the $conf parameter, which tells what and where to find data.
   * * `dim[X].Y` gets the Y value in $obs corresponding to $key X position
   * * `val[Z]` gets the value from $arv at Z position
   *
   * For instance, if $conf contains :
   * ```php
   * {
   *   "name": "dim[2].id",
   *   "label": "dim[2].name",
   *   "y": "val[0]"
   * }
   * ```
   *
   * Returns an array containing the requested data, eg.
   * ```php
   * [
   *   name: 1,
   *   label: 'Column 1',
   *   y: 97 
   * ]
   * ```
   *
   * @param string $key key from .stat eg. 0:0:1:0:3:0
   * @param array $arv array of values from .stat
   * @param array $conf array of key => parse definition
   * @param array $dims array of dimensions definitions provided by .stat
   * @param array $attrs array of attributes available
   *
   * @return array Parsed values extracted from $arv and $obs, following $conf definition
   */
  protected function parseRow($rowkey, $values, $conf, $dims, $attrs) {

    $data = [];

    // filtering
    if (!empty($conf['filter'])) {
      foreach ($conf['filter'] as $k => $p) {
        // filter format : [!]?value
        $neg = (strpos($p, '!') === 0)?true:false;
        $fv = ($neg)?substr($p, 1):$p;
        if ($fv == 'EMPTY') $fv = '';
        $fl = $this->parseKeyValue($k, $rowkey, $values, $dims, $attrs);
        if (!is_null($fl)) {
          if (
            (!$neg && $fv !== $fl) 
            || ($neg && $fv === $fl)
          ) {
            // filter do not match
            return false;
          }
        }
      }
    }

    // get value
    foreach ($conf['data'] as $k => $p) {
      $data[$k] = $this->parseKeyValue($p, $rowkey, $values, $dims, $attrs);
    }
    
    return $data;
  }
  
  /**
   * Returns parsed JSON results from .stat
   * This is called by DotStatChart block on submit
   *
   * @param string $url .stat API query URL
   * @param string $params parser method parameters
   *
   * @return array Returns array of "JS objects" containing data to visualize
   */
  public function getParsedResults($url, $params) {

    $res = $this->queryRaw($url);
    if (is_null($res)) {
      // request failed, most probably
      return null;
    }

    $raw = json_decode($res, true);

    if (empty($raw['data'])) {
      // no data
      \Drupal::logger('spc_dot_stat_data')->error('No data found in .stat API response for URL : ' . $url);
      return null;
    }

    $conf = json_decode($params, true);

    if (empty($conf['data'])) {
      // old style config backward compatibility
      $conf['data'] = $conf;
    }

    if (empty($conf['series'])) {
      // old style config backward compatibility
      $conf['series'] = [];
    }

    if (empty($conf['filter'])) {
      // old style config backward compatibility
      $conf['filter'] = [];
    }

    $data = [];

    if (array_key_exists('structure', $raw['data'])) {
      $structure = $raw['data']['structure'];
    } else if(array_key_exists('structures', $raw['data'])) {
      $structure = $raw['data']['structures'][$raw['data']['dataSets'][0]['structure']];
    }

    $dimensions = $structure['dimensions']['observation'];
    $attributes = $structure['attributes']['observation'];

    $observations = [];
    if (array_key_exists('dataSets', $raw['data'])) {
      if(array_key_exists('observations', $raw['data']['dataSets'][0])) {
        $observations = $raw['data']['dataSets'][0]['observations'];
      }
    }

    if (empty($conf['series'])) {

      // --------------- SINGLE DATA SET

      foreach ($observations as $key => $arv) {

        $res = $this->parseRow($key, $arv, $conf, $dimensions, $attributes);

        if (!$res) {
          // either empty or filtered
          continue;
        }

        if ($conf['data']['index']) {
          $idx = $res['index'];
          if (!empty($data[$idx]) && empty($res['value'])) {
            continue;
          }
          unset($res['index']);
          $data[$idx] = $res;
        } else {
          $data[] = $res;
        }
      }

      if ($conf['data']['index']) {
        ksort($data);
      }

      return json_encode(array('data' => $data), JSON_PRETTY_PRINT);

    } else {
      
      // --------------- MULTIPLE DATA SERIES

      // ----- PARSE SERIES CONFIG

      $confseries = [];

      if (is_string($conf['series'])) {
        // OLD METHOD (STRING) e.g. "dim[5].id, dim[2].id"
        $arrseries = explode(',', $conf['series']);
        foreach ($arrseries as $str) {
          $str = trim($str);
          $confseries[] = $str;
        }
      } else {
        // NEW METHOD (ARRAY) e.g. { "dim[2]", "dim[5]" }
        foreach ($conf['series'] as $str) {
          $confseries[] = $str;
        }
      }

      // ----- COMPILE SERIES STRUCTURE

      $dataseries = []; // dimensions (series) definitions

      // check filters
      $dimfilters = [];
      if (!empty($conf['filter'])) {
        foreach ($conf['filter'] as $k => $p) {
          if (!preg_match('/^dim\[[\d]+\]\.(id|name)$/', $k)) {
            continue; // series filters only apply to dimensions
          }
          $dimfilters[ $k ] = $p;
        }
      }

      foreach ($confseries as $sdef) {

        // parse index
        $tst = preg_match('/^(dim)\[(\d+)\](\.(.+))?$/', $sdef, $ari);
        if (!$tst) {
          // no index defined
          \Drupal::logger('spc_dot_stat_data')->error('SERIES MISCONFIGURATION: '.$sdef);
          continue;
        }

        // Series Dimensions
        $sidx = $ari[2]; // dim[X]
        $sidi = (isset($ari[4]))?$ari[4]:'id'; // dim[1].XXXX
        $sidn = 'name';

        // dimensions values
        $dims = $dimensions[$sidx]['values'];

        $dst = [];
        foreach ($dims as $dimidx => $dimobj) {

          $ok = true; // pass by default

          foreach ($dimfilters as $k => $p) {
            if ($k == 'dim['.$sidx.'].'.$sidi) {
              // value to test
              $neg = (strpos($p, '!') === 0)?true:false;
              $fv = ($neg)?substr($p, 1):$p;
              // matching key filter
              $fl = $dimobj[$sidi];
              if (!is_null($fl)) {
                if (
                  (!$neg && $fv != $fl) 
                  || ($neg && $fv == $fl)
                ) {
                  // filter do not match
                  $ok = false;
                  break;
                }
              }
            } 
          }
          
          if ($ok) {
            $dst[ $dimidx ] = [ 
              'id'    => $dimobj[ $sidi ],
              'name'  => $dimobj[ $sidn ]
            ];
          }
        }

        // dimensions definitions
        $dataseries[ $sidx ] = [
          'id'     => $dimensions[ $sidx ]['id'],
          'name'   => $dimensions[ $sidx ]['name'],
          'values' => $dst
        ];

      }

      // ----- COMPILE DATA (OBSERVATIONS)

      $test_keys = array_keys($dataseries);

      foreach ($observations as $key => $arv) {

        $tst = explode(':',$key);
        $res = $this->parseRow($key, $arv, $conf, $dimensions, $attributes);

        if (!$res) {
          // either empty or filtered
          continue;
        }

        switch(count($dataseries)) {
          case 1:
            $n = $tst[ $test_keys[0] ];
            $nlb = $dataseries[ $test_keys[0] ]['values'][$n];
            if (array_key_exists('index', $conf['data'])) {
              $idx = $res['index'];
              unset($res['index']);
              $data[ $nlb['id'] ][ $idx ] = $res;
            } else {
              $data[ $nlb['id'] ][] = $res;
            }
            break;
          case 2:
            $n = $tst[ $test_keys[0] ];
            $nlb = $dataseries[ $test_keys[0] ]['values'][$n];
            $m = $tst[ $test_keys[1] ];
            $mlb = $dataseries[ $test_keys[1] ]['values'][$m];

            if ($conf['data']['index']) {
              $idx = $res['index'];
              unset($res['index']);
              $data[ $nlb['id'] ][ $mlb['id'] ][ $idx ] = $res;
            } else {
              $data[ $nlb['id'] ][ $mlb['id'] ][] = $res;
            }
            
            break;
        }

      } // end loop rows

      // sort results by key
      if ($conf['data']['index']) {
        switch(count($dataseries)) {
          case 1:
            foreach($data as $k => $d) {
              ksort($d);
              $data[$k] = $d;
            }
            break;
          case 2:
            foreach($data as $k1 => $d1) {
              foreach($d1 as $k2 => $d2) {
                ksort($d2);
                $d1[$k2] = $d2;
              }
              $data[$k1] = $d1;
            }
            ksort($data);
            break;
        }
      }

      // loop over dataseries to remove items that have no data
      // had to be added because SDMX-JSON API returns now parent value for dimensions even if they do not have observation
      // cf. https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/324
      foreach ($dataseries as $k => $ds) {
        foreach ($ds['values'] as $n => $v) {
          if (!isset($data[$v['id']])) {
            unset($dataseries[$k]['values'][$n]);
          }
        }
      }

      // return JSON
      return json_encode(
        array(
          'series' => $dataseries,
          'data' => $data
        ),
        JSON_PRETTY_PRINT
      );
      
    }

    return json_encode($data);

  }

  
}