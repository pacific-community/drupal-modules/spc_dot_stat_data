<?php

/**
 * @file
 * Contains \Drupal\spc_dot_stat_data\Plugin\QueueWorker\DotStatQueueWorker.
 */

namespace Drupal\spc_dot_stat_data\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Processes tasks for spc_dot_stat_data module.
 *
 * @QueueWorker(
 *   id = "dot_stat_queue",
 *   title = @Translation("Dot Stat Queue worker"),
 *   cron = {"time" = 60}
 * )
 */
class DotStatQueueWorker extends QueueWorkerBase {
    
   private  $status = [];
    
  /**
   * {@inheritdoc}
   */
  public function processItem($entity) {
      
  // init .stat request service
    $service = \Drupal::service('spc_dot_stat_data.request');      
      
    $qry = $entity->getDataQuery();
    $parser = $entity->getDataParser();  

    
    $json = $service->getParsedResults($qry, $parser);

    if (empty($json)) {
      
      $this->status = 'error';
    
    } else if ($entity->getDataJson() != $json) {
    
      $entity->setDataJson($json);
      $entity->save();
      $this->status = 'update';
    
    } else {
    
      $this->status = 'skip';
    
    }
    
    \Drupal::logger('spc_dot_stat_data')->info('.Stat entity '. $entity->id() .' processed with status: ' . $this->status);
  }
  
}
