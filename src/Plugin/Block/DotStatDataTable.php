<?php

namespace Drupal\spc_dot_stat_data\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\spc_dot_stat_data\Entity\DotStatData;

/**
 * Displays Key Statistic in a table
 *
 * @Block(
 *   id = "dot_stat_data_table",
 *   admin_label = @Translation("PDH.stat Data Table"),
 *   category = @Translation("SPC .Stat"),
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Country"))
 *   }
 * )
 */
class DotStatDataTable extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'dot_stat_table_css' => 'table table-bordered table-striped table-hover table-xs',
      'dot_stat_table_header_css' => 'thead-dark'
    ];
  }
    
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    // custom table caption
    $form['dot_stat_table_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Table title (caption)'),
      '#size' => 64,
      '#default_value' => $config['dot_stat_table_title'],
      '#required' => false
    );
    
    // data source
    $form['dot_stat_table_node'] = array(
      '#type' => 'entity_autocomplete',
      '#target_type' => 'dot_stat_data',
      '#title' => t('Data Entity'),
      '#size' => 30,
      '#default_value' => isset($config['dot_stat_table_node']) ? DotStatData::load($config['dot_stat_table_node']) : null, // here's the previous value, if entered.
      '#required' => true
    );
    
    // data format
    $form['dot_stat_table_format'] = array(
      '#type' => 'select',
      '#title' => t('Data format'),
      '#options' => array(
         0 => t('Default'),
         1 => t('Number'),
         2 => t('Percentage'),
         3 => t('Rate'),
         4 => t('Yes/No')
      ),
      '#default_value' => $config['dot_stat_table_format']
    );

    // URL to .Stat : link to open the chart directly in .stat (new tab/window)
    $form['dot_stat_table_link'] = array(
      '#type' => 'url',
      '#title' => t('.Stat Link to source'),
      '#size' => 64,
      '#default_value' => $config['dot_stat_table_link'],
      '#required' => false,
      '#description' => 'Leave empty to use default link from Data Entity'
    );

    // custom column title
    $form['dot_stat_table_column_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Customize columns name'),
      '#size' => 64,
      '#default_value' => $config['dot_stat_table_column_name'],
      '#required' => false,
      '#description' => 'Separate columns by a pipe eg. "Countries|Population", or use "/Strip this/" to strip out some text from multiple column names (uses regexp)'
    );

    // custom table CSS classes
    $form['dot_stat_table_css'] = array(
      '#type' => 'textfield',
      '#title' => t('CSS class to add on TABLE'),
      '#size' => 32,
      '#default_value' => $config['dot_stat_table_css'],
      '#required' => false
    );

    // custom tablea header CSS class
    $form['dot_stat_table_header_css'] = array(
      '#type' => 'textfield',
      '#title' => t('CSS class to add on THEAD'),
      '#size' => 32,
      '#default_value' => $config['dot_stat_table_header_css'],
      '#required' => false
    );

    // other options
    $form['dot_stat_table_options'] = array(
      '#type' => 'textarea',
      '#title' => t('Options and customizations'),
      '#cols' => 64,
      '#rows' => 8,
      '#default_value' => $config['dot_stat_table_options'], 
      '#required' => false
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    
    parent::blockSubmit($form, $form_state);
    
    $values = $form_state->getValues();
    
    if (!$this->configuration['label_display']) {
      $node = \Drupal::entityTypeManager()->getStorage('dot_stat_data')->load($values['dot_stat_table_node']);
      $this->configuration['label'] = $node->label();
    }
    
    $this->configuration['dot_stat_table_title'] = $values['dot_stat_table_title'];
    $this->configuration['dot_stat_table_node'] = $values['dot_stat_table_node'];
    $this->configuration['dot_stat_table_format'] = $values['dot_stat_table_format'];
    $this->configuration['dot_stat_table_link'] = $values['dot_stat_table_link'];
    $this->configuration['dot_stat_table_column_name'] = $values['dot_stat_table_column_name'];
    $this->configuration['dot_stat_table_css'] = $values['dot_stat_table_css'];
    $this->configuration['dot_stat_table_header_css'] = $values['dot_stat_table_header_css'];
    $this->configuration['dot_stat_table_options'] = $values['dot_stat_table_options'];
    
  }

  protected function _parseValue($val, $cnf) {

    switch ($cnf['dot_stat_table_format']) {
      case 1: // number
        $val = number_format($val);
        break;
      case 2: // percent
        $val = $val.'%';
        break;
      case 2: // rate
        $val = (($val > 0)?'+':'').$val.'%';
        break;
      case 4: // Yes/No
        $val = ($val > 0)?'<span class="dot-stat-yes">Yes</span>':'<span class="dot-stat-no">No</span>';
        break;
      default:
        if (empty($val)) {
          $val = 'n/c';
        }
        break;
    }

    return $val;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    
    $config = $this->getConfiguration();
    $node = \Drupal::entityTypeManager()->getStorage('dot_stat_data')->load($config['dot_stat_table_node']);

    if (!$node) {
      return ['#markup' => '<div class="messages messages--error">'
        .'Node #'.$config['dot_stat_table_node'].' does not exist.'
        .'</div>'];
    }
    
    $title = $config['label'];

    // ---------- OPTIONS 
    $options = [
      'order'       => '',
      'index'       => '',
      'footer'      => '',
      'footer-css'  => ''
    ];
    if (!empty($config['dot_stat_table_options'])) {
      $cnf = json_decode($config['dot_stat_table_options'], true);
      $options = array_merge($options, $cnf);
    }
    
    // ---------- SERIES AND LABELS

    $series = $node->getSeriesJson();

    $rows = [
        'name'    => 'Items',
        'labels'  => [],
        'data'    => []
      ];

    if ($level1 = reset($series)) {

      $rows = [
        'name'    => $level1['name'],
        'labels'  => [],
        'data'    => []
      ];

      foreach ($level1['values'] as $obj) {
        $rows['labels'][ $obj['id'] ] = $obj['name'];
      }

    }

    // ---------- PARSE DATA

    // check depth
    $depth = $node->getDataDepth();

    // get data
    $data = $node->getKeyData();
    
    // start parsing
    $cols = array();

    switch ($depth) {

      case 0:
        // simple table (single column)
        $cols[0] = 'Data';
        foreach ($data as $k => $col) {
          $rows['labels'][$k] = $col['name'];
          $rows['data'][$k][0] = $col;
        }
        ksort($rows);
        break;

      case 1:
        // data series (multiple columns)
        foreach ($data as $k => $rowdata) {
          foreach ($rowdata as $colkey => $coldata) {
            if (!array_key_exists($colkey, $cols)) {
              $cols[$colkey] = isset($coldata['name'])?$coldata['name']:$colkey;
            }
            $arv = $coldata;
            $rows['data'][$k][$colkey] = $arv;
          }

        }

    }

    if ($options['order']) {
      // force order
      if (strpos($options['order'], ',') === false) {
        switch ($options['order']) {
          case 'desc':
            arsort($cols);
            break;
          default:
            asort($cols);
            break;
        }
      } else {
        $order = explode(',',$options['order']);
        $newcols = [];
        foreach($order as $col) {
          $newcols[$col] = $cols[$col];
        }
        $cols = $newcols;
      }
    }

    // --------------- LINK TO .STAT

    $url = $node->getDataLink();
    if (!empty($config['dot_stat_table_link'])) {
      if ($config['dot_stat_table_link'] == '#') {
        $url = '';
      } else {
        $url = $config['dot_stat_table_link'];
      }
    }
    
    // ---------- GENERATE HTML

    $align = 'text-align-left';
    switch ($config['dot_stat_table_format']) {
      case 1:
      case 2:
      case 3:
        $align = 'text-align-right';
        break;
      case 4:
        $align = 'text-align-center';
        break;
    }

    // --- INIT CAPTION FIELD 

    $caption = $field =  $format = '';

    if (!empty($config['dot_stat_table_title'])) {

      $caption = $config['dot_stat_table_title'];

      if (preg_match('/{{([a-z0-9_\-]+)(\|[^}]+)?}}/', $caption, $arr)) {
        $field = $arr[1];
        $format = empty($arr[2])?'':substr($arr[2],1);
      }

    }

    // --- ROWS

    ksort($rows['data']); // re-order by column header

    $fieldmin = $fieldmax = ''; // store caption field min and max (if requested)
    $totals = array_fill_keys(array_keys($cols), 0);

    $htmlrows = []; // rendered table rows

    foreach ($rows['data'] as $key => $coldata) {
      $html = '<tr>';
      if ($options['index']) {
        $html .= '<th class="text-nowrap">'.$key.'</th>';
      }
      $html .= '<th class="text-nowrap">'.$rows['labels'][$key].'</th>';
      foreach ($cols as $k => $n) {
        $tdv = '<td></td>';
        if (array_key_exists($k, $coldata)) {
          // column values
          $arr = $coldata[$k];

          $tdv = '<td';
          // title requested (tooltip)
          if (!empty($arr['title'])) {
            $tdv .= ' title="'.$arr['title'].'" data-toggle="tooltip" data-placement="top"';
          }
          $tdv .= ' class="'.$align.'">'.$this->_parseValue($arr['value'], $config).'</td>';

          // get field values (min and max)
          if (!empty($field) && !empty($arr[$field])) {
            $fieldmin = empty($fieldmin)?$arr[$field]:min($fieldmin, $arr[$field]);
            $fieldmax = empty($fieldmax)?$arr[$field]:max($fieldmax, $arr[$field]);
          }
        }
        $html .= $tdv;
        // totals
        if (isset($arr['value']) && is_numeric($arr['value'])) {
          $totals[$k] += floatval($arr['value']);
        }
      }
      $html .= '</tr>';
      $htmlrows[] = $html;
    }

    // --- TABLE & HEADERS

    // show last only
      if (preg_match('/\|?last/', $format)) {
        $format = preg_replace('/\|?last/','', $format);
        $fieldmin = $fieldmax;
      }

    // HTML
    
    $pf = '<div class="table-responsive">'
      .'<table class="stat-data-table '.$config['dot_stat_table_css'].'">';
    
    if (!empty($config['dot_stat_table_title'])) {

      $caption = $config['dot_stat_table_title'];

      // replace field value
      if ($field && $fieldmin) {
        $str = $fieldmin;
        switch($field) {
          case 'date':
            $srv = \Drupal::service('date.formatter');
            $format = empty($format)?'F j, Y':$format;
            $str = $srv->format(strtotime($fieldmin), 'custom', $format);
            if ($fieldmin != $fieldmax) {
              $str .= ' - '.$srv->format(strtotime($fieldmax), 'custom', $format);
            }
            break;
          default:
            if ($fieldmin != $fieldmax) {
              $str .= ' - '.$fieldmax;
            }
            break;
        }
        $caption = preg_replace('/{{[^}]+}}/', $str, $caption);
      }

      // add link (if any)
      if (!empty($url)) {
        $str = '<a href="'.$url.'"';
        if (strpos($url, 'http') === 0) {
          $str .= ' target="_blank"';
        }
        $str .= '>'.$caption.'</a>';
        $caption = $str;
      }

      // add HTML caption tag
      $pf .= '<caption>'.$caption.'</caption>';
    }

    $headertitle = $rows['name'];
    $arrcoltitles = [];
    if (strpos($config['dot_stat_table_column_name'], '/') === false 
      && strpos($config['dot_stat_table_column_name'], '|') > 0) 
    {
      $arrcoltitles = explode('|', $config['dot_stat_table_column_name']);
      $headertitle = array_shift($arrcoltitles);
    }

    $pf .= '<thead class="'.$config['dot_stat_table_header_css'].'"><tr>'
      .'<th class="text-align-left"'
      .($options['index']?' colspan="2"':'')
      .'>'.$headertitle.'</th>';
    foreach ($cols as $title) {
      $tooltip = '';
      if ($config['dot_stat_table_column_name']) {
        if (strpos($config['dot_stat_table_column_name'], '/') === 0) {
          $title = ucfirst(trim(preg_replace($config['dot_stat_table_column_name'], '', $title)));
        } else if (count($arrcoltitles) > 0) {
          $title = array_shift($arrcoltitles);
        } else {
          $title = $config['dot_stat_table_column_name'];
        }
      }
      $pf .= '<th class="text-align-center"'
        .($tooltip?(' title="'.$tooltip.'" data-toggle="tooltip" data-placement="top"'):'')
        .'>'.ucfirst($title).'</th>';
    }        
    $pf .= '</tr></thead>'
      .'<tbody>';

    $sf = '</tbody>';

    // --- FOOTER TOTAL
    if (!empty($options['footer'])) {
      $sf .= '<tfoot>'
        .'<tr'.(empty($options['footer-css'])?'':(' class="'.$options['footer-css'].'"')).'>'
        .'<td'.($options['index']?' colspan="2"':'').'>'
        . (is_string($options['footer'])?$options['footer']:'Total')
        .'</td>';
      foreach($totals as $v) {
        $sf .= '<td class="'.$align.'">'.$this->_parseValue($v, $config).'</td>';
      }
      $sf .= '</tr></tfoot>';
    }

    // --- CLOSE TABLE

    $sf .= '</table></div>';

    // --------------- JS & CSS LIBRARIES

    $settings = \Drupal::config('spc_dot_stat_data.settings');

    $libraries = [
      'spc_dot_stat_data/dotstatdata', // Highcharts.js library
    ];
    
    if ($bootversion = $settings->get('bootstrap')) {
      array_unshift($libraries, 'spc_dot_stat_data/'.$bootversion);
    }
    
    // --------------- RETURN MARKUP

    return [
      '#markup' => implode("\n", $htmlrows),
      '#prefix' => $pf,
      '#suffix' => $sf,
      '#attached' => [
        'library' => $libraries
      ],
      '#cache' => [
        'max-age' => 3600, //caching chart for an hour
        'tags' => [ "node:{$node->id()}", "dotstat:table" ] // data entity dependent
      ]
    ];
    
  }

}
