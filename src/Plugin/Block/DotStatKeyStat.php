<?php

namespace Drupal\spc_dot_stat_data\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\spc_dot_stat_data\Entity\DotStatData;

/**
 * Displays Key Statistic fetched from SPC .stat
 *
 * @Block(
 *   id = "dot_stat_key_stat",
 *   admin_label = @Translation("PDH.stat Key Statistic"),
 *   category = @Translation("SPC .Stat"),
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Country"))
 *   }
 * )
 */
class DotStatKeyStat extends BlockBase implements BlockPluginInterface {
    
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    // load list of icons
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('spc_dot_stat_data')->getPath().'/assets/icons';

    $arf = \Drupal::service('file_system')->scanDirectory($module_path, '/\.(png|jpg|gif|jpeg)$/', [ 'key'=>'filename' ]);
    $arr_icons = array();
    foreach ($arf as $fn => $ff) {
      $fn = substr($fn, 0, strrpos($fn, '.'));
      $arr_icons [ $fn ] = ucwords(str_replace('_', ' ', $fn));
    }

    /*
    $form['dot_stat_key_country'] = array(
      '#type' => 'url',
      '#title' => t('.Stat Data query URL'),
      '#size' => 80,
      '#default_value' => $config['dot_stat_key_country'],
      '#required' => true
    );
    */
    
    $form['dot_stat_key_node'] = array(
      '#type' => 'entity_autocomplete',
      '#target_type' => 'dot_stat_data',
      '#title' => t('Data Entity'),
      '#size' => 30,
      '#default_value' => isset($config['dot_stat_key_node']) ? DotStatData::load($config['dot_stat_key_node']) : null, // here's the previous value, if entered.
      '#required' => true
    );

    // Link to open 
    $form['dot_stat_key_link'] = array(
      '#type' => 'linkit',
      '#title' => t('Link (optional)'),
      '#size' => 80,
      '#default_value' => $config['dot_stat_key_link'],
      '#required' => false,
      '#description' => 'Leave empty to use default link from Data Entity, or enter "#" to force no link'
    );
    
    $form['dot_stat_key_format'] = array(
      '#type' => 'select',
      '#title' => t('Data format'),
      '#options' => array(
         0 => t('Default'),
         1 => t('Number'),
         2 => t('Percent/Rate'),
         3 => t('Currency USD'),
         4 => t('Text')
      ),
      '#default_value' => $config['dot_stat_key_format']
    );

    $form['dot_stat_key_date'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display date'),
      '#default_value' => isset($config['dot_stat_key_date']) ? $config['dot_stat_key_date'] : 1
    );

    $form['dot_stat_key_function'] = array(
      '#type' => 'select',
      '#title' => t('Calculation function'),
      '#options' => array(
         0 => t('Value/Total'),
         1 => t('Average')
      ),
      '#default_value' => $config['dot_stat_key_function']
    );

    // Icon
    $form['dot_stat_key_icon'] = array(
      '#type' => 'select',
      '#title' => t('Icon (optional)'),
      '#options' => $arr_icons,
      '#required' => false,
      '#empty_option' => '---',
      '#default_value' => $config['dot_stat_key_icon']
    );

    // Unit
    $form['dot_stat_key_unit'] = array(
      '#type' => 'textfield',
      '#title' => t('Unit (optional)'),
      '#size' => 10,
      '#default_value' => $config['dot_stat_key_unit'],
      '#required' => false
    );

    // Total or random country
    $form['dot_stat_key_country'] = array(
      '#type' => 'select',
      '#title' => t('Country selection (if not on a country page)'),
      '#options' => array(
         0 => t('Total'),
         1 => t('Randomly picked'),
         2 => t('Choose country')
      ),
      '#default_value' => $config['dot_stat_key_country']
    );

    $form['dot_stat_key_country_value'] = array(
      '#type' => 'select',
      '#title' => t('Country value'),
      '#options' => [
        'AS' => t('American Samoa'),
        'CK' => t('Cook Islands'),
        'FJ' => t('Fiji'),
        'FM' => t('Micronesia (Federated States of)'),
        'GU' => t('Guam'),
        'KI' => t('Kiribati'),
        'MH' => t('Marshall Islands'),
        'MP' => t('Northern Mariana Islands'),
        'NC' => t('New Caledonia'),
        'NR' => t('Nauru'),
        'NU' => t('Niue'),
        'PF' => t('French Polynesia'),
        'PG' => t('Papua New Guinea'),
        'PN' => t('Pitcairn Islands'),
        'PW' => t('Palau'),
        'SB' => t('Solomon Islands'),
        'TK' => t('Tokelau'),
        'TO' => t('Tonga'),
        'TV' => t('Tuvalu'),
        'VU' => t('Vanuatu'),
        'WF' => t('Wallis and Futuna'),
        'WS' => t('Samoa')
      ],
      '#empty_options' =>  '---',
      '#states' => [
        'visible' => [
          ':input[name$="[field_block_reference][0][settings][dot_stat_key_country]"]' => ['value' => 2],
        ],
      ],
      '#default_value' => $config['dot_stat_key_country_value']
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    
    parent::blockSubmit($form, $form_state);
    
    $values = $form_state->getValues();
    
    if (!$this->configuration['label_display']) {
      $node = \Drupal::entityTypeManager()->getStorage('dot_stat_data')->load($values['dot_stat_key_node']);
      $this->configuration['label'] = $node->label();
    } else {
      // $this->configuration['label_display'] = false;
    }

    $this->configuration['dot_stat_key_node'] = $values['dot_stat_key_node'];
    $this->configuration['dot_stat_key_format'] = $values['dot_stat_key_format'];
    $this->configuration['dot_stat_key_function'] = $values['dot_stat_key_function'];
    $this->configuration['dot_stat_key_unit'] = $values['dot_stat_key_unit'];
    $this->configuration['dot_stat_key_date'] = $values['dot_stat_key_date'];
    $this->configuration['dot_stat_key_link'] = $values['dot_stat_key_link'];
    $this->configuration['dot_stat_key_icon'] = $values['dot_stat_key_icon'];
    $this->configuration['dot_stat_key_country'] = $values['dot_stat_key_country'];
    $this->configuration['dot_stat_key_country_value'] = $values['dot_stat_key_country_value'];
    
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    
    $config = $this->getConfiguration();

    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('spc_dot_stat_data')->getPath();
    
    $node = \Drupal::entityTypeManager()->getStorage('dot_stat_data')->load($config['dot_stat_key_node']);

    $session = \Drupal::request()->getSession();

    $title = $config['label'];
    
    /** @var \Drupal\node\NodeInterface $country */
    $country = $this->getContextValue('node');

    $legend_prefix = [];
    $legend_separator = '';
    
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getName();
    
    $cnt = 0;

    if ($country && $country->field_iso2_code) {
      // get key data
      $data = $node->getKeyData($country->get('field_iso2_code')->value);
    } else {
      $all = $node->getKeyData();
      if($config['dot_stat_key_country'] == 0) {
        // get it all
        $data = [
          'value' => 0,
          'first' => 9999,
          'year' => 0
        ];

        foreach ($all as $k => $a) {
          $cnt++;
          $data['value'] += $a['value'];
          if (!empty($a['year'])) {
            $data['first'] = min($data['first'], $a['year']);
            $data['year'] = max($data['year'], $a['year']);
          }
        }
        $legend_prefix[] = 'All Pacific';
        $legend_separator = ', ';
      } else if ($config['dot_stat_key_country'] == 1) {
        // get a random item of all
        $cnt = &drupal_static(__FUNCTION__);
        if(empty($cnt)) {
          $cnt = array_keys($all)[rand(0, count($all) - 1)];
        }
        if(isset($all[$cnt])) {
          $data = $all[$cnt];
          $legend_prefix[] = $data['country'];
        } else {
          $data = [
            'value' => NULL,
          ];
        }
        $legend_separator = ', ';
      } else {
        $data = $all[$config['dot_stat_key_country_value']];
      }
    }
    
    $value = $data['value'];
    
    if (!empty($config['dot_stat_key_function'])) {
      switch ($config['dot_stat_key_function']) {
        case '1':
          // average
          $value = round($value / $cnt);
          $legend_prefix[] = '('.t('Average').')';
          break;
      }
    }

    $icf = '';
    if (!empty($config['dot_stat_key_icon'])) {
      $icf = '<div class="stat-group--icon">'
       .'<img src="/'.$module_path.'/assets/icons/'.$config['dot_stat_key_icon'].'.png" />'
       .'</div>';
    }

    if (isset($value) && !empty($data['year'])) {
      if ($lang == "FR") {
        $decimal_sep = ',';
        $thousand_sep = ' ';
      } else {
        $decimal_sep = '.';
        $thousand_sep = ',';
      }
      if ($config['dot_stat_key_format'] != 4 && is_numeric($value)) {
        $value = rtrim(rtrim(number_format($value, 2, $decimal_sep, $thousand_sep), 0), $decimal_sep);
      }
      switch ($config['dot_stat_key_format']) {
        case 1: // number
          break;
        case 2: // rate
          $value = (($value > 0)?'+':'').$value.'%';
          break;
        case 3: // currency ($)
          $value = '$'.$value;
          break;
        case 4: // text
          break;
      }
    } else {
      $value = 'n/c';
    }
    
    $pf = '<div class="stat-group--tile">';
    $sf = '</div>';

    // default url from 
    $url = $node->getDataLink();
    if (!empty($config['dot_stat_key_link'])) {
      if ($config['dot_stat_key_link'] == '#') {
        $url = '';
      } else {
        $url = $config['dot_stat_key_link'];
      }
    }
    
    if (!empty($url) && $value != 'n/c') {
      $pf = '<a href="'.$url.'"';
      if (strpos($url, 'http') === 0) {
        $pf .= ' target="_blank"';
      }
      $pf .= ' class="stat-group--tile">';
      $sf = '</a>';
    }

    if (!empty($data['first']) && $data['first'] != $data['year']) {
      $legend_separator .= $data['first'].'-';
    }

    $markup_value = '<div class="stat-group--nc">n/c</div>';
    if ($value != 'n/c') {
      $markup_value = '<div class="stat-group--value">'.$value;
      if (isset($config['dot_stat_key_unit'])) {
        $markup_value = $markup_value.' '.$config['dot_stat_key_unit'];
      }
      $markup_value = $markup_value.'</div>';
      if ($config['dot_stat_key_date'] == 1) {
        $markup_value = $markup_value.'<div class="stat-group--date">'.implode(' ',$legend_prefix).$legend_separator.$data['year'].'</div>';
      }
    }
    
    // return markup
    return [
      '#title' => false,
      '#markup' => 
        '<div class="stat-group--container">'
          .$icf
          .'<div class="stat-group--body">'
            .'<div class="stat-group--label">'.$title.'</div>'
            .$markup_value
          .'</div>'
        .'</div>',
      '#prefix' => $pf,
      '#suffix' => $sf,
      '#cache' => [
        'max-age' => 0, //caching chart for an hour
        'tags' => [ "node:{$node->id()}", "dotstat:keystat" ] // data entity dependent
      ]        
    ];
    
  }

}