<?php

namespace Drupal\spc_dot_stat_data\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining SPC .Stat Data entities.
 *
 * @ingroup spc_dot_stat_data
 */
interface DotStatDataInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the SPC .Stat Data name.
   *
   * @return string
   *   Name of the SPC .Stat Data.
   */
  public function getName();

  /**
   * Sets the SPC .Stat Data name.
   *
   * @param string $name
   *   The SPC .Stat Data name.
   *
   * @return \Drupal\spc_dot_stat_data\Entity\DotStatDataInterface
   *   The called SPC .Stat Data entity.
   */
  public function setName($name);
  
  /**
   * Gets the SPC .Stat Data Query.
   *
   * @return string
   */
  public function getDataQuery();
  
  /**
   * Sets the SPC .Stat Data Query.
   */
  public function setDataQuery($uri);
  
  /**
   * Gets the SPC .Stat Data parser parameters.
   *
   * @return string
   */
  public function getDataParser();
  
  /**
   * Sets the SPC .Stat Data parser parameters.
   */
  public function setDataParser($config);
  
  /**
   * Gets the SPC .Stat Default Link.
   *
   * @return string
   */
  public function getDataLink();
  
  /**
   * Sets the SPC .Stat Default Link.
   */
  public function setDataLink($uri);
  
  /**
   * Gets the SPC .Stat Data Query.
   *
   * @return string
   */
  public function getDataJson();
  
  /**
   * Sets the SPC .Stat Data Query.
   */
  public function setDataJson($uri);


  /**
   * Gets the SPC .Stat Data creation timestamp.
   *
   * @return int
   *   Creation timestamp of the SPC .Stat Data.
   */
  public function getCreatedTime();

  /**
   * Sets the SPC .Stat Data creation timestamp.
   *
   * @param int $timestamp
   *   The SPC .Stat Data creation timestamp.
   *
   * @return \Drupal\spc_dot_stat_data\Entity\DotStatDataInterface
   *   The called SPC .Stat Data entity.
   */
  public function setCreatedTime($timestamp);


}
