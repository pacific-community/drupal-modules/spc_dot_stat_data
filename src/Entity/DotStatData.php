<?php

namespace Drupal\spc_dot_stat_data\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the SPC .Stat Data entity.
 *
 * @ingroup spc_dot_stat_data
 *
 * @ContentEntityType(
 *   id = "dot_stat_data",
 *   label = @Translation(".Stat Data Entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\spc_dot_stat_data\DotStatDataListBuilder",
 *     "views_data" = "Drupal\spc_dot_stat_data\Entity\DotStatDataViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\spc_dot_stat_data\Form\DotStatDataForm",
 *       "add" = "Drupal\spc_dot_stat_data\Form\DotStatDataForm",
 *       "edit" = "Drupal\spc_dot_stat_data\Form\DotStatDataForm",
 *       "delete" = "Drupal\spc_dot_stat_data\Form\DotStatDataDeleteForm",
 *     },
 *     "access" = "Drupal\spc_dot_stat_data\DotStatDataAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\spc_dot_stat_data\DotStatDataHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "dot_stat_data",
 *   admin_permission = "view .stat data entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/dot_stat_data/{dot_stat_data}",
 *     "add-form" = "/admin/content/dot_stat_data/add",
 *     "edit-form" = "/admin/content/dot_stat_data/{dot_stat_data}/edit",
 *     "delete-form" = "/admin/content/dot_stat_data/{dot_stat_data}/delete",
 *     "collection" = "/admin/content/dot_stat_data",
 *   },
 *   field_ui_base_route = "dot_stat_data.settings"
 * )
 */
class DotStatData extends ContentEntityBase implements DotStatDataInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getDataQuery() {
    return $this->get('data_query')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDataQuery($uri) {
    $this->set('data_query', $uri);
    return $this;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getDataParser() {
    return $this->get('data_parser')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDataParser($config) {
    $this->set('data_parser', $config);
    return $this;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getDataLink() {
    return $this->get('data_link')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDataLink($uri) {
    $this->set('data_link', $uri);
    return $this;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getDataJson() {
    return $this->get('data_json')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDataJson($data) {
    $this->set('data_json', $data);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the SPC .Stat Data entity.'))
      ->setSettings([
        'max_length' => 128,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);
    
    $fields['data_query'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Query URI'))
      ->setDescription(t('URI to query data from .Stat API'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'uri_link',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'uri',
        'weight' => 5,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);
    
    $fields['data_parser'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Parser configuration'))
      ->setDescription(t('Configuration parameters to parse .Stat results'))
      ->setSettings([
        'max_length' => 999,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'text',
        'weight' => 10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'settings' => [
          'cols' => 40,
          'rows' => 8,
          'resizable' => 'vertical'
        ],
        'weight' => 10,
      ])
      ->setDefaultValue("{\n  \"series\": [],"
        ."\n  \"data\": {"
        ."\n    \"index\": \"dim[X].id\","
        ."\n    \"name\": \"dim[Y].name\","
        ."\n    \"value\": \"value\""
        ."\n  }\n}")
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);
    
    $fields['data_link'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Default Link'))
      ->setDescription(t('URL to .Stat Data Explorer'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'uri_link',
        'weight' => 15,
      ])
      ->setDisplayOptions('form', [
        'type' => 'uri',
        'weight' => 15,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);
    
    $fields['data_json'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Query Results'))
      ->setDescription(t('Parsed data results'))
      ->setSettings([
        'max_length' => 4096,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'text',
        'weight' => 20,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);
    
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the SPC .Stat Data entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 25,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 25,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);
    
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * get json series definitions
   */
  public function getSeriesJson() {
    $json = json_decode($this->getDataJson(), true);
    $jseries = empty($json['series'])?[]:$json['series'];
    return $jseries;
  }
  
  /**
   * get json data
   *
   * @param $idx get data by index (or all data if undefined) 
   */
  public function getKeyData($idx='') {
    $json = json_decode($this->getDataJson(), true);
    $jdata = empty($json['data'])?$json:$json['data'];
    if ($idx) {
      if (array_key_exists($idx, $jdata)) {
        return $jdata[$idx];
      } else {
        return [
          'value' => 0,
          'first' => 9999,
          'year' => 0
        ];
      }
    } else {
      return $jdata;
    }
  }

  /**
   * check depth of data
   */
  public function getDataDepth() {
    $jseries = $this->getSeriesJson();
    return count($jseries);
  }

}
