(function ($) {
  Drupal.behaviors.dotStatMap = {
    attach: function (context, settings) { 
        
      DotStatMap = function(id, opts, specs, title, subtitle, link) {

        /**
         * Constructor
         */
        this.id = id;
        this.title = title;
        this.subtitle = subtitle;

        var obj = this;
        
        // define common / default options
        this.options = {
        	chart: {
        		spacing: [5,5,0,0]
        	},
          title: {
            useHTML: true,
            text: obj.title
          },
          subtitle: {
          	useHTML: true,
					  text: obj.subtitle
					},
          exporting: {
			      buttons: {
			        contextButton: {
			          menuItems: ["viewFullscreen", "printChart", "separator", "downloadPNG", "downloadJPEG", "downloadPDF"]
			        }
			      },
			      sourceWidth: 1600,
			      sourceHeight: 1000
			    },
			    mapNavigation: {
			      enabled: true,
			      buttonOptions: {
			        verticalAlign: 'bottom'
			      },
			      buttons: {
			      	zoomIn: {
			      		x: 5,
			      		y: -5
			      	},
			      	zoomOut: {
			      		x: 5,
			      		y: 23
			      	}
			      }
			    },
			    plotOptions: {
				    map: {
				      dataLabels: {
				       format: '{point.label}',
				        allowOverlap: true
				      }
				    }
				  }
        };

        // link ?
        if (link) {
					this.options.exporting.menuItemDefinitions = {
					  sourceLink: {
					    text: 'View Data in PDH.stat',
					    onclick: function() {
					      window.open(link.replace(/&amp;/g, "&"), '_blank');
					    }
					  }
					};
					this.options.exporting.buttons.contextButton.menuItems.unshift('sourceLink');
				}

        // extend options with block settings
        $.extend(true, this.options, specs, opts);

	      /**
	       * Initialize HTML structure (DOM)
	       */
	      this.initHtml = function() {
	        var $d = $('#'+this.id);
	        if (!$d[0]) {
	          alert('ERR: Target div #'+this.id+' not found');
	          return false;
	        }

	        if (this.options.chart.height) {
	        	$d.attr('style', 'height:'+this.options.chart.height+'px !important;overflow:hidden;');
	        	this.options.chart.height = null;
	        }
	        
	        return true;
	      };

	      /**
	       * Initial chart rendering
	       */
	      this.render = async function() {

	        var obj = this;

			const world = await fetch(obj.world).then(response => response.json());
			const geomap = await fetch(obj.geomap).then(response => response.json());


	        this.options.series = [{
				type: 'map',
				data: Highcharts.geojson(world, 'map'),
				name: 'World',
				color: '#ffcc99'
			}, {
				data: Highcharts.geojson(geomap, 'map'),
				keys: 'id',
				type: 'map',
				joinBy: ['id','Code'],
				name: 'PICTS EEZ',
				color: "rgba(51,125,201,.09)",
				dataLabels: {
					format: '{point.name}',
					enabled: true,
					color: 'rgb(51,51,51)',
					position: 'right'
				},
				enableMouseTracking: false
			}, {
				data: this.rawdata,
				mapData: geomap,
				type: 'mapbubble',
				joinBy: ['id','Code'],
				name: 'Data',
				stickyTracking: false
			}];

			obj.options.mapView = {
				center: [3401073, -933210],
				zoom: -12.6
			}

			obj.map = Highcharts.mapChart(obj.id, obj.options);

	      };

	      /**
	       * GO: this where it all starts
	       * This is the public method to be called right after object has been instanciated
	       * (might be overloaded e.g. adding drill down data)
	       */
	      this.go = function(geomap, data) {
	      	this.geomap = geomap;
			// get JSON world file which is in the same directory that geomap
			this.world = geomap.split('/');
			this.world.splice(-1, 1, "world.json")
			this.world = this.world.join('/');
	        this.rawdata = data;
	        if (this.initHtml()) {
	          this.render();
	        }
	      };

	    }

    }
  };        
})(jQuery);
