<?php

namespace Drupal\spc_dot_stat_map\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\spc_dot_stat_data\Entity\DotStatData;

/**
 * Displays Chart created from .stat data
 *
 * @Block(
 *   id = "dot_stat_map",
 *   admin_label = @Translation("PDH.stat Map"),
 *   category = @Translation("SPC .Stat")
 * )
 */


class DotStatMap extends BlockBase implements BlockPluginInterface {
  
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    // load list of configuration templates
    
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('spc_dot_stat_map')->getPath();

    $arf = \Drupal::service('file_system')->scanDirectory($module_path.'/maps', '/\.json$/', [ 'key'=>'filename' ]);
    $arr_map_options = array();
    foreach ($arf as $fn => $ff) {
      $fn = substr($fn, 0, strrpos($fn, '.'));
      $fs = preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", $fn);
      $arr_map_options[ $fn ] = ucwords(trim($fs));
    }

    // Chart title
    $form['dot_stat_map_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Chart title (main heading)'),
      '#size' => 64,
      '#default_value' => $config['dot_stat_map_title'],
      '#required' => false
    );

    // Chart subtitle
    $form['dot_stat_map_subtitle'] = array(
      '#type' => 'textfield',
      '#title' => t('Chart subtitle (smaller heading)'),
      '#size' => 64,
      '#default_value' => $config['dot_stat_map_subtitle'],
      '#required' => false
    );

    // Node to get Data from
    $form['dot_stat_map_node'] = array(
      '#type' => 'entity_autocomplete',
      '#target_type' => 'dot_stat_data',
      '#title' => t('Data Entity'),
      '#size' => 48,
      '#default_value' => isset($config['dot_stat_map_node']) ? DotStatData::load($config['dot_stat_map_node']) : null, // here's the previous value, if entered.
      '#required' => false
    );

    // JS Data array configuration
    $form['dot_stat_map_geojson'] = array(
      '#type' => 'select',
      '#title' => t('Map (GeoJSON)'),
      '#default_value' => $config['dot_stat_map_geojson'], 
      '#required' => true,
      // '#empty_option' => t('- No Map -'),
      '#options' => $arr_map_options
    );
    
    // StatChart and Highchart options
    $form['dot_stat_map_conf'] = array(
      '#type' => 'textarea',
      '#title' => t('HighCharts Configuration (JSON Object)'),
      '#cols' => 64,
      '#rows' => 8,
      '#default_value' => $config['dot_stat_map_conf'], 
      '#required' => false
    );

    // URL to .Stat : link to open the chart directly in .stat (new tab/window)
    $form['dot_stat_map_link'] = array(
      '#type' => 'url',
      '#title' => t('.Stat Link to source'),
      '#size' => 64,
      '#default_value' => $config['dot_stat_map_link'],
      '#required' => false,
      '#description' => 'Leave empty to use default link from Data Entity'
    );

    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    
    $this->configuration['dot_stat_map_node'] = $values['dot_stat_map_node'];
    $this->configuration['dot_stat_map_title'] = $values['dot_stat_map_title'];
    $this->configuration['dot_stat_map_subtitle'] = $values['dot_stat_map_subtitle'];
    $this->configuration['dot_stat_map_link'] = $values['dot_stat_map_link'];
    $this->configuration['dot_stat_map_geojson'] = $values['dot_stat_map_geojson'];
    $this->configuration['dot_stat_map_conf'] = $values['dot_stat_map_conf'];
    
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    
    $config = $this->getConfiguration();

    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('spc_dot_stat_map')->getPath();

    // --------------- CHECK DATA ENTITY

    $node = false; // No data

    if (!empty($config['dot_stat_map_node'])) {
      // node: .stat entity containing json data
      $node = \Drupal::entityTypeManager()->getStorage('dot_stat_data')->load($config['dot_stat_map_node']);
    }

    // check node is defined
    if (!$node) {
      return ['#markup' => '<div class="messages messages--error">'
        .'Node #'.$config['dot_stat_map_node'].' does not exist.'
        .'</div>'];
    }
    
    // --------------- LINK TO .STAT

    $url = false;

    if (!empty($config['dot_stat_map_link'])) {
      if ($config['dot_stat_map_link'] == '#') {
        $url = '';
      } else {
        $url = $config['dot_stat_map_link'];
      }
    } else if ($node) {
      $url = $node->getDataLink();
    }

    // --------------- SETTING THINGS UP FOR VISUALISATIION
    
    // highcharts custom configuration    
    $conf = empty($config['dot_stat_map_conf'])?"{}":$config['dot_stat_map_conf'];

    $confSpecific = "{}";

    $confparse = empty($config['dot_stat_map_conf'])?[]:json_decode(preg_replace('/function\(\) {[^}]+}/','""', $conf), true);
    
    // use block title
    $dtitle = $config['label'];
    
    // generate unique DIV ID attribute
    $did = 'dotstatmap-'.md5($dtitle);
    
    // JS variable unique name (DotStatChart object)
    $xid = str_replace('-','_', $did);


    // --------------- GEO MAP (JSON)

    $geomap = $config['dot_stat_map_geojson'];
    
    // check class is defined
    if (!file_exists($module_path.'/maps/'.$geomap.'.json')) {
      return ['#markup' => '<div class="messages messages--error">'
        .'Map &laquo; '.$geomap.' &raquo; can not be found.'
        .'</div>'];
    }
    
    $geomap = '/'.$module_path.'/maps/'.$geomap.'.json';


    // --------------- PARSE CATEGORIES AND DATA

    // --- INIT CAPTION FIELD 

    $label = $caption = $field = $format = '';

    if (!empty($config['dot_stat_map_title']) && preg_match('/{{([a-z0-9_\-]+)(\|[^}]+)?}}/', $config['dot_stat_map_title'], $arr)) {
      $label = 'dot_stat_map_title';
      $caption = $config['dot_stat_map_title'];
      $field = $arr[1];
      $format = empty($arr[2])?'':substr($arr[2],1);
    } else if (!empty($config['dot_stat_map_subtitle']) && preg_match('/{{([a-z0-9_\-]+)(\|[^}]+)?}}/', $config['dot_stat_map_subtitle'], $arr)) {
      $label = 'dot_stat_map_subtitle';
      $caption = $config['dot_stat_map_subtitle'];
      $field = $arr[1];
      $format = empty($arr[2])?'':substr($arr[2],1);
    }

    // --- COUNTRIES
    
    $countries = [];
    $rawseries = $node->getSeriesJson();
    foreach ($rawseries[1]['values'] as $cnty) {
      $countries[ $cnty['id'] ] = $cnty['name'];
    }

    // --- DATA

    $data = [];

    $fieldmin = $fieldmax = ''; // store caption field min and max (if requested)
    
    $rawdata = $node->getKeyData();

    // @todo check if this is a COVID map

    $confSpecific = "{
      colorAxis: {
        dataClasses: [{
          to: 1,
          name: 'New cases reported in the past 14 days',
          color: 'rgba(200,0,0,.6)'
        }, {
          from: 2,
          to: 14,
          name: 'No new cases for at least 14 days',
          color: 'rgba(255,101,0,.6)'
        }, {
          from: 15,
          to: 28,
          name: 'No new cases for at least 28 days',
          color: 'rgba(255,210,0,.6)'
        }, {
          from: 29,
          name: 'No new cases for at least 42 days',
          color: 'rgba(0,201,51,.6)'
        }]
      }
    }";

    foreach ($rawdata as $cid => $cnty) {
      /* echo "<pre>------------ $cid : {$countries[$cid]} \n\n";
      var_dump($cnty);
      echo '</pre>'; */
      $obj = new \stdClass;
      $obj->Code = $cid;
      $obj->label = $countries[$cid];
      if (!isset($cnty['CASES']) || !isset($cnty['CASES14D']) || !isset($cnty['CASES28D']) || !isset($cnty['CASES42D'])) {
        // data entity does not contain expected data
        return ['#markup' => '<div class="messages messages--error">'
          .'Node #'.$config['dot_stat_map_node'].' is not compatible with this map.'
          .'</div>'];
        continue;
      }
      $obj->z = $cnty['CASES']['value'];
      $obj->date = $cnty['CASES']['date'];

      if (!empty($field) && !empty($obj->$field)) {
        $fieldmin = empty($fieldmin)?$obj->$field:min($fieldmin, $obj->$field);
        $fieldmax = empty($fieldmax)?$obj->$field:max($fieldmax, $obj->$field);
      }

      if (intval($cnty['CASES14D']['value']) > 0) {
        $obj->color = 'rgba(200,0,0,.6)'; // red
        $obj->desc = $cnty['CASES14D']['value'].' case'.(intval($cnty['CASES14D']['value'])>1?'s':'').' in the past 14 days';
      } else if (intval($cnty['CASES28D']['value']) > 0) {
        $obj->color = 'rgba(255,101,0,.6)'; // orange
        $obj->desc = $cnty['CASES28D']['value'].' case'.(intval($cnty['CASES28D']['value'])>1?'s':'').' in the past 28 days';
      } else if (intval($cnty['CASES42D']['value']) > 0) {
        $obj->color = 'rgba(255,210,0,.6)'; // yellow
        $obj->desc = $cnty['CASES42D']['value'].' case'.(intval($cnty['CASES42D']['value'])>1?'s':'').' in the past 42 days';
      } else {
        $obj->color = 'rgba(0,201,51,.6)'; // green
        $obj->desc = 'No new cases for at least 42 days';
      }
      $data[] = $obj;
    }

    // --- FIELD VALUE in TITLES

    // show last only
    if (preg_match('/\|?last/', $format)) {
      $format = preg_replace('/\|?last/','', $format);
      $fieldmin = $fieldmax;
    }
    
    if (!empty($label)) {

      $caption = $config[$label];

      // replace field value
      if ($field) {
        $str = $fieldmin;
        switch($field) {
          case 'date':
            $srv = \Drupal::service('date.formatter');
            $format = empty($format)?'F j, Y':$format;
            $str = $srv->format(strtotime($fieldmin), 'custom', $format);
            if ($fieldmin != $fieldmax) {
              $str .= ' - '.$srv->format(strtotime($fieldmax), 'custom', $format);
            }
            break;
          default:
            if ($fieldmin != $fieldmax) {
              $str .= ' - '.$fieldmax;
            }
            break;
        }
        $config[$label] = preg_replace('/{{[^}]+}}/', $str, $caption);
      }
    }
    

    // --------------- JS & CSS LIBRARIES

    $settings = \Drupal::config('spc_dot_stat_data.settings');

    $libraries = [
      'spc_dot_stat_chart/highcharts',
      'spc_dot_stat_map/highcharts-maps'
      // 'spc_dot_stat_map/highcharts-maps-ext'
    ];
    
    
    // Bootstrap too ?
    if ($bootversion = $settings->get('bootstrap')) {
      array_unshift($libraries, 'spc_dot_stat_data/'.$bootversion);
    }

    // More JS functions from main module
    $libraries[] ='spc_dot_stat_data/dotstatdata';


    // --------------- JAVASCRIPT INLINE CODE

    $datajson = json_encode($data, JSON_PRETTY_PRINT);


    $script = "$xid = new DotStatMap(
      \"$did\", "
      .$conf
      .','.$confSpecific
      .", \"".str_replace('"','\'',$config['dot_stat_map_title'])."\""
      .", \"".str_replace('"','&quote;',$config['dot_stat_map_subtitle'])."\""
      .", \"".($url?str_replace('"','',$url):'')."\""
      .");\n";

    $script .= "\n$xid.go(\n"
      ."'{$geomap}',\n"
      .json_encode($data, JSON_NUMERIC_CHECK)
      ."\n);";
    
    // wait for every external JS to be loaded (Jquery and Highchart) before rendering
    $script = "var $xid = false;\nwindow.addEventListener('load', function() {\n".$script."\n});";

    // --------------- GENERATE MARKUP

    // return markup, including :
    // - DIV to append chart in
    // - Aniimated loader image (SVG)
    // - JS code to instanciate DotStatChart object
    return [
      '#markup' => 
        // open DIV to which the generated chart will be added
        '<div id="'.$did.'" class="highcharts-map">'
        .'<svg viewBox="0 0 135 140" xmlns="http://www.w3.org/2000/svg" class="highcharts-loader"><rect x="0" y="5" width="15" height="140" rx="6"><animate attributeName="height" begin="0s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect><rect x="30" y="50" width="15" height="100" rx="6"><animate attributeName="height" begin="0.25s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.25s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect><rect x="60" y="25" width="15" height="120" rx="6"><animate attributeName="height" begin="0.5s" dur="1s" values="90;80;70;60;45;30;140;120;120;110;100" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.5s" dur="1s" values="55;65;75;85;100;115;5;25;25;35;45" calcMode="linear" repeatCount="indefinite" /></rect><rect x="90" y="30" width="15" height="120" rx="6"><animate attributeName="height" begin="0.25s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.25s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect><rect x="120" y="70" width="15" height="80" rx="6"><animate attributeName="height" begin="0.5s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.5s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect></svg>'
        .'</div>'."\n"
        // insert Javascript inline (@todo should go to the end of <body>, after JS includes)
        .'<script>'.$script.'</script>',
      '#attached' => [
        'library' => $libraries
      ],
      '#allowed_tags' => ['div','svg','rect','animate','script','em','strong','b','br','style'],
      '#cache' => [
        'max-age' => 3600, //caching chart for an hour
        'tags' => [ "node:{$node->id()}", "dotstat:map" ] // data entity dependent
      ]
    ];
    
  }

}