(function ($) {
  Drupal.behaviors.dotStatChartPopulationPyramidMulti = {
    attach: function (context, settings) { 
        
      /**
       * Population Pyramid Multi set
       * with SELECT selector on top
       * DEPRECATED - TO BE DELETED
       */
      DotStatChartPopulationPyramidMulti = function(id, categories, otpl, opts, title, subtitle, link) {

        // inherit from super class
        DotStatChart.call(this, id, categories, otpl, opts, title, subtitle, link);

        /**
         * Initial chart rendering
         * (overloading default method)
         */
        this.render = function() {

          var obj = this;

          this.datasets = [];
          for (var kn in this.rawdata) {
            this.datasets.push(kn);
          }
          this.dbg('MULTI PYRAMID:');
          this.dbg(this.datasets);
          var first = this.datasets[0];
          if (this.options.dotStatChartOptions.defaultSet) {
            first = this.options.dotStatChartOptions.defaultSet;
          }

          this.options.series = [ {
            name: 'Male',
            data: $.map(this.rawdata[first]['Male'], function(a) { return 0 - a ; }),
            color: '#9cf'
          }, {
            name: 'Female',
            // data: [...this.rawdata[first]['Female']],
            data: $.map(this.rawdata[first]['Female'], function(a) { return a; }),
            color: '#f9c'
          } ];

          // Generate SELECT HTML code
          var htmlswitch = '<select class="spc-play-pyramid" onchange="'+this.id.replace('-','_')+'.switch(this.value)">';
          for (var j=0; j<this.datasets.length; j++) {
            htmlswitch += '<option value="'+this.datasets[j]+'"';
            if (first == this.datasets[j]) {
              htmlswitch += ' selected="selected"';
            }
            htmlswitch += '>'+this.datasets[j]+'</option>';
          }
          htmlswitch += '</select>'
            +'<button type="button" onclick="'+this.id.replace('-','_')+'.animate(this)"></button>';

          // Render 
          obj.dbg('Rendering...');
          obj.dbg(this.options);

          this.chart = Highcharts.chart(this.id+'-chart', this.options, function() {
            if (obj.options.showTable ) {
              $('#'+obj.id+'-data-table', context).html(this.getTable());
            }  
          });

          // Add Year selector
          $('#'+this.id, context).prepend('<div class="spc-chart-switching-buddy">'+htmlswitch+'</div>');

        } // end render

        /**
         * Switch Year (Population Pyramid multi specific)
         */
        this.switch = function(val) {
          if (this.animating) {
            window.clearInterval(this.animating);
            $('#'+this.id+' .spc-chart-switching-buddy button', context).removeClass('pause');
            this.animating = false;
          }
          this.chart.series[0].setData($.map( this.rawdata[val]['Male'], function(a) { return 0 - a ; }));
          this.chart.series[1].setData(this.rawdata[val]['Female']);
        }

        this.update = function(data, options) {
          if (options) {
            this.chart.series[0].update(options, false);
          }
          // update raw data and switch to default set
          this.rawdata = response.data;
          $('#'+this.id+' select.spc-play-pyramid', context).val(this.options.dotStatChartOptions.defaultSet);
          this.switch(this.options.dotStatChartOptions.defaultSet);
        }

        /**
         * Animate Population Pyramid (Multi)
         * Deprecated: template will be removed soon
         */
        this.animate = function(elb) {
          var obj = this;
          if (this.animating) {
            window.clearInterval(this.animating);
            $(elb, context).removeClass('pause');
            this.animating = false;
          } else {
            $(elb, context).addClass('pause');
            this.animating = window.setInterval(function() {
              var els = $('#'+obj.id+' select.spc-play-pyramid', context);
              var opts = [];
              var cv = els.val();
              var nx = 0;
              var is = false;
              els.children('option').map(function(idx, opt) {
                opts.push(opt.value);
                if (is && !nx) {
                  nx = idx;
                }
                if (cv == opt.value) {
                  is = true;
                }
              });
              els.prop('selectedIndex', nx);
              var val = els.val();
              obj.chart.series[0].setData($.map(obj.rawdata[val]['Male'], function(a) { return 0 - a ; }));
              obj.chart.series[1].setData(obj.rawdata[val]['Female']);
            }, obj.options.dotStatChartOptions.speed);
          }
        }
      }
      
    }
  }
})(jQuery);