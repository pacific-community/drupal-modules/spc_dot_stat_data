(function ($) {
  Drupal.behaviors.dotStatChartDrilldown = {
    attach: function (context, settings) { 
        
      /**
       * Population Pyramid Race
       */        
      DotStatChartDrilldown = function(id, categories, otpl, opts, title, subtitle, link) {

        // inherit from super class
        DotStatChart.call(this, id, categories, otpl, opts, title, subtitle, link);

        /**
        * Initial chart rendering
        * (overloading default method)
        */
        this.render = function() {

          var obj = this;

          this.options.series = [ { name: "Countries", type: 'column', data: this.rawdata } ];

          this.options.drilldown.series = this.drilldata;

          obj.dbg('Rendering...');
          obj.dbg(this.options);

          this.chart = Highcharts.chart(this.id+'-chart', this.options, function() {
            if (obj.options.showTable ) {
              $('#'+obj.id+'-data-table', context).html(this.getTable());
            }  
          });

        } // end render

      }
        
    }
  }        
})(jQuery);