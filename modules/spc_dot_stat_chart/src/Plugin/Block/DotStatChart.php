<?php

namespace Drupal\spc_dot_stat_chart\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\spc_dot_stat_data\Entity\DotStatData;

/**
 * Displays Chart created from .stat data
 *
 * @Block(
 *   id = "dot_stat_chart",
 *   admin_label = @Translation("PDH.stat Chart"),
 *   category = @Translation("SPC .Stat")
 * )
 */


class DotStatChart extends BlockBase implements BlockPluginInterface {
  
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    // load list of configuration templates
    
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('spc_dot_stat_chart')->getPath().'/src/Charts';

    $arf = \Drupal::service('file_system')->scanDirectory($module_path, '/\.php$/', [ 'key'=>'filename' ]);
    $arr_templates_options = array();
    foreach ($arf as $fn => $ff) {
      $fn = substr($fn, 0, strrpos($fn, '.'));
      $fs = preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", $fn);
      $arr_templates_options [ $fn ] = ucwords(trim($fs));
    }

    // Chart title
    $form['dot_stat_chart_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Chart title (main heading)'),
      '#size' => 64,
      '#default_value' => $config['dot_stat_chart_title'],
      '#required' => false
    );

    // Chart subtitle
    $form['dot_stat_chart_subtitle'] = array(
      '#type' => 'textfield',
      '#title' => t('Chart subtitle (smaller heading)'),
      '#size' => 64,
      '#default_value' => $config['dot_stat_chart_subtitle'],
      '#required' => false
    );

    // Node to get Data from
    $form['dot_stat_chart_node'] = array(
      '#type' => 'entity_autocomplete',
      '#target_type' => 'dot_stat_data',
      '#title' => t('Data Entity'),
      '#size' => 48,
      '#default_value' => isset($config['dot_stat_chart_node']) ? DotStatData::load($config['dot_stat_chart_node']) : null, // here's the previous value, if entered.
      '#required' => false
    );

    // JS Data array configuration
    $form['dot_stat_chart_template'] = array(
      '#type' => 'select',
      '#title' => t('HighCharts Configuration Template'),
      '#default_value' => $config['dot_stat_chart_template'], 
      '#required' => false,
      '#empty_option' => t('- No Visualisation -'),
      '#options' => $arr_templates_options
    );
    
    // StatChart and Highchart options
    $form['dot_stat_chart_conf'] = array(
      '#type' => 'textarea',
      '#title' => t('HighCharts Configuration (JSON Object)'),
      '#cols' => 64,
      '#rows' => 8,
      '#default_value' => $config['dot_stat_chart_conf'], 
      '#required' => false
    );

    // URL to .Stat : link to open the chart directly in .stat (new tab/window)
    $form['dot_stat_chart_link'] = array(
      '#type' => 'url',
      '#title' => t('.Stat Link to source'),
      '#size' => 64,
      '#default_value' => $config['dot_stat_chart_link'],
      '#required' => false,
      '#description' => 'Leave empty to use default link from Data Entity'
    );

    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    
    $this->configuration['dot_stat_chart_node'] = $values['dot_stat_chart_node'];
    $this->configuration['dot_stat_chart_title'] = $values['dot_stat_chart_title'];
    $this->configuration['dot_stat_chart_subtitle'] = $values['dot_stat_chart_subtitle'];
    $this->configuration['dot_stat_chart_link'] = $values['dot_stat_chart_link'];
    $this->configuration['dot_stat_chart_template'] = $values['dot_stat_chart_template'];
    $this->configuration['dot_stat_chart_conf'] = $values['dot_stat_chart_conf'];
    
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    
    $config = $this->getConfiguration();

    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('spc_dot_stat_chart')->getPath();

    // --------------- CHECK DATA ENTITY

    $node = false; // No data

    if (!empty($config['dot_stat_chart_node'])) {
      // node: .stat entity containing json data
      $node = \Drupal::entityTypeManager()->getStorage('dot_stat_data')->load($config['dot_stat_chart_node']);
    }
    
    // --------------- LINK TO .STAT

    $url = false;

    if (!empty($config['dot_stat_chart_link'])) {
      if ($config['dot_stat_chart_link'] == '#') {
        $url = '';
      } else {
        $url = $config['dot_stat_chart_link'];
      }
    } else if ($node) {
      $url = $node->getDataLink();
    }

    // --------------- NO DATA / VISUALISATION

    if (!$node || empty($config['dot_stat_chart_template'])) {

      $suffix = '';
      $label = $node?$node->label():$config['label'];
        
      if (preg_match("/^SDG ([a-z\.\d]+)/", $label, $arm) ) {
        $suffix = ' for indicator '.$arm[1];
      }

      $datalink = 'No data is currently available'.$suffix;
      if ($url) {
        $datalink = '<a href="'.$url.'" target="_blank">'
          .'View data'.$suffix.' on PDH.stat'
          .'</a>';
      }

      $html = '<div class="spc-dot-stat-no-chart media my-3">'
        .'<img alt="" class="mr-3" src="/'.$module_path.'/img/icon-no-visualisation.png">'
        .'<div class="media-body">'
        .'<p><em>Visualisation(s) not yet available</em><br />'
        .$datalink
        .'</p>'
        .'</div>'
        .'</div>';

      return [
          '#markup' => $html,
          '#cache' => [
            'max-age' => 3600, //caching chart for 1 hour.
          ]
        ];
    }

    // --------------- SETTING THINGS UP FOR VISUALISATIION
    
    // highcharts custom configuration    
    $conf = empty($config['dot_stat_chart_conf'])?"false":$config['dot_stat_chart_conf'];
    
    // use block title
    $dtitle = $config['label'];
    
    // generate unique DIV ID attribute
    $did = 'dotstat-'.md5($dtitle);
    
    // JS variable unique name (DotStatChart object)
    $xid = str_replace('-','_', $did);


    // --------------- CHART TEMPLATE (CLASS)

    $tpl = $config['dot_stat_chart_template'];
    
    // check class is defined
    if (!file_exists($module_path.'/src/Charts/'.$tpl.'.php')) {
      return ['#markup' => '<div class="messages messages--error">'
        .'Template &laquo; '.$tpl.' &raquo; is not defined.'
        .'</div>'];
    }
    
    $tplclass = "Drupal\\spc_dot_stat_chart\\Charts\\{$tpl}";

    $objTemplate = new $tplclass(
      $node->getSeriesJson(),
      $node->getKeyData(),
      $node->getDataDepth()
    );

    // -------------- HIGHCHARTS DEFAULT CONFIGURATION

    $settings = \Drupal::config('spc_dot_stat_chart.settings');

    // get config from settings
    $tplstr = strtolower(preg_replace('/(?<=\\w)(?=[A-Z])/',"_$1", $tpl));
    $template = $settings->get($tplstr);

    if (empty($template)) {
      // use default highcharts configuration from template
      $template = $objTemplate->getHighchartsOptions();
    }

    // --------------- PARSE CONF

    $confparse = empty($template)?[]:json_decode(preg_replace('/function\(\).+\}/','""', $template), true);

    if (!empty($config['dot_stat_chart_conf'])) {
      if ($jparse = json_decode(preg_replace('/function\(\).+\}/','""', $config['dot_stat_chart_conf']), true)) {
        $confparse = array_replace_recursive($confparse, $jparse);
      }
    }

    $objTemplate->setOptions($confparse);

    // --------------- PARSE CATEGORIES AND DATA

    $rows = array();
    
    $categories = $objTemplate->getCategories();

    $rawdata = $objTemplate->getData();

    if (
      !empty($confparse['dotStatChartOptions']['orderByValues'])
      && $confparse['dotStatChartOptions']['orderByValues'] == 'DESC'
    ) {

      // echo '<pre>'; print_r($categories); echo '</pre>';
      // echo '<hr /><pre>'; print_r($rawdata); echo '</pre>';
      
      $merged = [];
      foreach($categories as $i => $c) {
        $merged[] = [
          'cat' => $c,
          'd1'  => $rawdata[0]['data'][$i],
          'd2'  => $rawdata[1]['data'][$i]
        ];
      }

      // echo '<hr /><pre>'; print_r($merged); echo '</pre>';

      $columns_1 = array_column($merged, 'd1');
      $columns_2 = array_column($merged, 'd2');
      array_multisort($columns_2, SORT_DESC, $columns_1, SORT_DESC, $merged);

      // echo '<hr /><pre>'; print_r($merged); echo '</pre>';

      $categories = array_column($merged, 'cat');
      $rawdata[0]['data'] = array_column($merged, 'd1');
      $rawdata[1]['data'] = array_column($merged, 'd2');

    }

    $drilldata = $objTemplate->getDrillData();


    // --------------- CAPTION FIELD

    $label = $caption = $field = $format = '';
    $fieldmin = $fieldmax = ''; // store caption field min and max (if requested)

    if (!empty($config['dot_stat_chart_title']) && preg_match('/{{([a-z0-9_\-]+)(\|[^}]+)?}}/', $config['dot_stat_chart_title'], $arr)) {
      $label = 'dot_stat_chart_title';
      $field = $arr[1];
      $format = empty($arr[2])?'':substr($arr[2],1);
    } else if (!empty($config['dot_stat_chart_subtitle']) && preg_match('/{{([a-z0-9_\-]+)(\|[^}]+)?}}/', $config['dot_stat_chart_subtitle'], $arr)) {
      $label = 'dot_stat_chart_subtitle';
      $field = $arr[1];
      $format = empty($arr[2])?'':substr($arr[2],1);
    }

    if (!empty($field)) {

      $rowdepth = $node->getDataDepth();

      switch ($rowdepth) {
        case 0:
          foreach ($rawdata as $cid => $point) {
            if (!empty($point[$field])) {
              $fieldmin = empty($fieldmin)?$point[$field]:min($fieldmin, $point[$field]);
              $fieldmax = empty($fieldmax)?$point[$field]:max($fieldmax, $point[$field]);
            }
          }
          break;
        case 1:
          foreach ($rawdata as $cid => $serie) {
            if (array_key_exists($field, $serie)) {
              $fieldmin = empty($fieldmin)?$serie[$field]:min($fieldmin, $serie[$field]);
              $fieldmax = empty($fieldmax)?$serie[$field]:max($fieldmax, $serie[$field]);
            } else {
              foreach ($serie as $point) {
                if (!empty($point[$field])) {
                  $fieldmin = empty($fieldmin)?$point[$field]:min($fieldmin, $point[$field]);
                  $fieldmax = empty($fieldmax)?$point[$field]:max($fieldmax, $point[$field]);
                }
              }
            }
          }
          break;
      } // end case

      // show last only
      if (preg_match('/\|?last/', $format)) {
        $format = preg_replace('/\|?last/','', $format);
        $fieldmin = $fieldmax;
      }

      // --- FIELD VALUE in TITLES

      $caption = $config[$label];

      // replace field value
      $str = $fieldmin;
      
      switch($field) {
        case 'date':
          $timestamp = strtotime($fieldmin);
          if (is_integer($timestamp)) {
            $srv = \Drupal::service('date.formatter');
            $format = empty($format)?'F j, Y':$format;
            $str = $srv->format($timestamp, 'custom', $format);
            if (is_integer($fieldmax) && $fieldmin != $fieldmax) {
              $str .= ' - '.$srv->format(strtotime($fieldmax), 'custom', $format);
            }
          } else if ($fieldmin != $fieldmax) {
            $str .= ' - '.$fieldmax;
          }
          break;
        default:
          if ($fieldmin != $fieldmax) {
            $str .= ' - '.$fieldmax;
          }
          break;
      }

      $config[$label] = preg_replace('/{{[^}]+}}/', $str, $caption);

    } // end if field


    // --------------- CHECK FOR MULTIPLE DATA SOURCES

    $sourcesel = [];
    $sourcecur = 0;

    if (!empty($confparse['dotStatChartOptions']) && !empty($confparse['dotStatChartOptions']['liveDataFilter'])) {
      $entity_storage = \Drupal::entityTypeManager()->getStorage('dot_stat_data');
      $entity_ids = $entity_storage->getQuery()
        ->condition('name', $confparse['dotStatChartOptions']['liveDataFilter'], 'LIKE')
        ->sort('name', 'ASC')
        ->execute();

      foreach ($entity_ids as $nid) {
        $nlabel = $entity_storage->load($nid)->label();
        if (!empty($confparse['dotStatChartOptions']['liveDataRegexp'])) {
          if (preg_match($confparse['dotStatChartOptions']['liveDataRegexp'], $nlabel, $matches)) {
            $nlabel = trim($matches[1]);
          }
        }
        $sourcesel[$nlabel] = $nid;
        if ($nid == $config['dot_stat_chart_node']) {
          $sourcecur = $nid;
        }
      }

    }
    

    // --------------- JS & CSS LIBRARIES

    $settings = \Drupal::config('spc_dot_stat_data.settings');

    $libraries = [
      'spc_dot_stat_chart/highcharts', // Highcharts.js library
    ];
    
    // specific highcharts modules
    foreach ($objTemplate->getHighchartsModules() as $lib) {
      $libraries[] = $lib;
    }

    // Main .stat charts JS class
    $libraries[] ='spc_dot_stat_chart/statchart';

    // Add more libraries from Template 
    foreach ($objTemplate->getAdditionalLibraries() as $lib) {
      $libraries[] = $lib; 
    }
    
    // Bootstrap too ?
    if ($bootversion = $settings->get('bootstrap')) {
      array_unshift($libraries, 'spc_dot_stat_data/'.$bootversion);
    }

    // More JS functions from main module
    $libraries[] ='spc_dot_stat_data/dotstatdata';


    // --------------- JAVASCRIPT INLINE CODE

    // Main Chart Object class to use e.g. DotStatChart (by default)
    $jsclass = $objTemplate->getJavascriptClass();
    
    // instanciate JS DotStatChart object
    $script = "$xid = new ".$jsclass."(
      \"$did\", "
      .(is_array($categories)?json_encode(array_values($categories)):"\"$categories\"")
      .", "
      .$template
      .", "
      .$conf
      .", \"".str_replace('"','\'',$config['dot_stat_chart_title'])."\""
      .", \"".str_replace('"','&quote;',$config['dot_stat_chart_subtitle'])."\""
      .", \"".($url?str_replace('"','',$url):'')."\""
      .");\n";
    
    $script .= "\n$xid.go(\n"
      .json_encode($rawdata, JSON_NUMERIC_CHECK).",\n"
      .json_encode($drilldata, JSON_NUMERIC_CHECK)
      ."\n);";

    if (count($sourcesel) > 0) {
      $script .="\n$xid.altsource(".json_encode($sourcesel, JSON_FORCE_OBJECT).", ".$sourcecur.");\n";
    }
    
    // wait for every external JS to be loaded (Jquery and Highchart) before rendering
    $script = "var $xid = false; window.addEventListener('load', function() {\n".$script."\n});";


    // --------------- GENERATE MARKUP

    $tpl = strtolower(preg_replace('/(?<=\d)(?=[A-Za-z])|(?<=[A-Za-z])(?=\d)|(?<=[a-z])(?=[A-Z])/', '-', $tpl));

    // style options
    $cstyle = '';
    $astyle = [];
    
    if (!empty($confparse['dotStatChartOptions']['style'])) {
      foreach ($confparse['dotStatChartOptions']['style'] as $k => $v) {
        $astyle[$k] = ($k . ': '. $v);
      }
      $cstyle = '<style>#'.$did.' .highcharts-chart {'
        .implode("; ", $astyle)
        .'; }</style>';
    }

    // return markup, including :
    // - DIV to append chart in
    // - Aniimated loader image (SVG)
    // - JS code to instanciate DotStatChart object
    return [
      '#markup' => 
        // open DIV to which the generated chart will be added
        $cstyle
        .'<div id="'.$did.'" class="highcharts-'.$tpl.'">'
        // add animated loader (SVG)
        .'<div class="highcharts-chart">'
        .'<svg viewBox="0 0 135 140" xmlns="http://www.w3.org/2000/svg" class="highcharts-loader"><rect x="0" y="5" width="15" height="140" rx="6"><animate attributeName="height" begin="0s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect><rect x="30" y="50" width="15" height="100" rx="6"><animate attributeName="height" begin="0.25s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.25s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect><rect x="60" y="25" width="15" height="120" rx="6"><animate attributeName="height" begin="0.5s" dur="1s" values="90;80;70;60;45;30;140;120;120;110;100" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.5s" dur="1s" values="55;65;75;85;100;115;5;25;25;35;45" calcMode="linear" repeatCount="indefinite" /></rect><rect x="90" y="30" width="15" height="120" rx="6"><animate attributeName="height" begin="0.25s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.25s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect><rect x="120" y="70" width="15" height="80" rx="6"><animate attributeName="height" begin="0.5s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.5s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect></svg>'
        .'</div>'
        // close DIV
        .'</div>'."\n"
        // insert Javascript inline (@todo should go to the end of <body>, after JS includes)
        .'<script>'.$script.'</script>',
      '#attached' => [
        'library' => $libraries
      ],
      '#allowed_tags' => ['div','svg','rect','animate','script','em','strong','b','br','style'],
      '#cache' => [
        'max-age' => 3600,
        'tags' => [ "node:{$node->id()}", "dotstat:chart" ] // data entity dependent
      ]

    ];
    
  }

}