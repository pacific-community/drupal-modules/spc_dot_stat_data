<?php

/**
 * Population Pyramid Multi set
 * with SELECT selector on top
 * DEPRECATED - TO BE DELETED
 */

namespace Drupal\spc_dot_stat_chart\Charts;

class PopulationPyramidMulti extends \Drupal\spc_dot_stat_chart\DotStatChartTemplate {

  public function __construct($series, $data, $depth) {
    parent::__construct($series, $data, $depth);
  }

  public function getCategories() {

    $cats = [];

    // in this case, categories are age ranges 
    // coming from dataset, not from series definition

    $dcopy = $this->data;

    // assuming all series use the same dimensions (same age ranges)
    $d1 = array_shift($dcopy); // first year on dataset
    $d2 = array_shift($d1); // Gender (F or M) dataset

    foreach ($d2 as $k => $arv) {
      $cats[$k] = $arv['age'];
    }
    
    return $cats;
  }

  public function getData() {
    // expecting SERIES depth 2:
    // 1) TIME PERIOD (year)
    // 2) GENDER (F/M)
    $fulldata = [];
    foreach ($this->data as $ky => $arr) {
      $series = array_keys($arr);
      $datayear = [];
      foreach($series as $ks) {
        $data = [];
        foreach ($arr[$ks] as $k => $arv) {
          $data[] = $arv['value'];
        }
        $datayear[ $ks ] = $data;
      }
      $fulldata[ $ky ] = $datayear;
    }
    return $fulldata;
  }

	public function getHighchartsOptions() {
		return <<<EOS
{
  "chart": {
    "type": "bar",
    "backgroundColor": "#fff"
  },
  "xAxis": [{
    "categories": [],
    "opposite": false,
    "reversed": false,
    "labels": {
      "step": 1
    }
  }, {
    "categories": [],
    "opposite": true,
    "reversed": false,
    "linkedTo": 0,
    "labels": {
      "step": 1
    }
  }],
  "yAxis": {
    "title": {
      "text": null
    },
    "labels": {
      "formatter": function() { return Math.abs(this.value); }
    }
  },
  "plotOptions": {
    "series": {
      "stacking": true,
      "pointPadding": 0,
      "groupPadding": 0,
      "borderWidth": 1
    }
  },
  "tooltip": {
    "formatter": function() { return '<b>' + this.series.name + ', Age ' + this.point.category + '</b><br/>' + 'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 0); }
  },
  "legend": {
    "enabled": false
  },
  "exporting": {
    "buttons": {
      "contextButton": {
        "menuItems": ["viewFullscreen","printChart","separator","downloadPNG","downloadPDF","downloadSVG"]
      }
    }
  },
  "credits": { "enabled": false },
  "dotStatChartOptions": {
    "rawDataType": "population_pyramid_multi",
    "speed": 500
  }
}
EOS;
	}

  /**
   * Get Javascript class name
   */
  public function getAdditionalLibraries() {
    return [ 
      'spc_dot_stat_chart/statchart-population-pyramid-multi'
    ];
  }

  /**
   * Get Javascript class name
   */
  public function getJavascriptClass() {
    return 'DotStatChartPopulationPyramidMulti';
  }

}