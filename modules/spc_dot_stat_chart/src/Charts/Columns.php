<?php

/**
 * Column chart
 */

namespace Drupal\spc_dot_stat_chart\Charts;

class Columns extends \Drupal\spc_dot_stat_chart\DotStatChartTemplate {

  public function __construct($series, $data, $depth) {
    parent::__construct($series, $data, $depth);
  }

  /**
   * Get Highchart formatted dataset from harvested data saved in Drupal entity
   *
   * @return array of data
   */
  public function getData() {
    $rawdata = [];
    switch ($this->depth) {
    case 0:
      // --------------- NO SERIES
      foreach ($this->data as $key => $arv) {
        $rawdata[] = $this->_json2data($key, $arv);
      }
      break;
    case 1:
      // --------------- 1-LEVEL SERIES
      $s1 = reset($this->series);
      foreach($s1['values'] as $sdef) {
        $data = [];
        $sid = $sdef['id'];
        foreach ($this->data[ $sid ] as $key => $arv) {
          // overwrite to keep only last value
          $data = $this->_json2data($key, $arv);
        }
        $data['name'] = $sid;
        $data['label'] = $sdef['name'];
        $rawdata[$sid] = $data;
      }
      ksort($rawdata);
      $rawdata = array_values($rawdata);
    }

    // return array of objects of data
    $rawobj = (object) [
      'data' => $rawdata
    ];
    return [ $rawobj ];
  }

	public function getHighchartsOptions() {
		return <<<EOS
{
  "chart": {
    "type": "column",
    "backgroundColor": "#fff",
    "height": 300
  },
  "xAxis": {
    "type": "category"
  },
  "yAxis": {
    "title": { "text": "" }
  },
  "legend": {
    "enabled": false
  },
  "plotOptions": {
    "series": {
      "pointPadding": 0.1,
      "groupPadding": 0,
      "borderWidth": 1,
      "maxPointWidth": 70,
      "dataLabels": {
        "enabled": false
      }
    }
  },
  "credits": {
    "enabled": false
  },
  "dotStatChartOptions": {
    "rawDataType": "columns"
  }
}
EOS;

	}

}