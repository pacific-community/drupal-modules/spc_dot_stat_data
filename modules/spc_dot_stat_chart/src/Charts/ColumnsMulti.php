<?php

/**
 * Column chart
 */

namespace Drupal\spc_dot_stat_chart\Charts;

class ColumnsMulti extends \Drupal\spc_dot_stat_chart\DotStatChartTemplate {

  public function __construct($series, $data, $depth) {
    parent::__construct($series, $data, $depth);
  }

  public function getCategories() {
    $categories = [];

    // in this case, categories are names from series definition
    $level1 = reset($this->series);
    foreach ($level1['values'] as $obj) {
      $categories[ $obj['id'] ] = $obj['name'];
    }

    // sort by index
    ksort($categories);

    return array_values($categories);
  }

  /**
   * Get Highchart formatted dataset from harvested data saved in Drupal entity
   *
   * @return array of data
   */
  public function getData() {

    // show only specific values
    $colselection = [];
    if (!empty($this->config['dotStatChartOptions']['values'])) {
      $colselection = $this->config['dotStatChartOptions']['values'];
    }

    // define groups
    $fulldata = [];
    $fdata = reset($this->data);
    foreach ($fdata as $k => $arr) {
      if (empty($colselection) || in_array($k, $colselection)) {
        // error_log("creating serie $k : ".$arr['name']);
        $fulldata[$k] = [
          'name' => $arr['name'],
          'data' => []
        ];
        if (!empty($this->config['dotStatChartOptions'][$k.'_inside'])) {
          $fulldata[$k]['dataLabels'] = [ 'inside' => $this->config['dotStatChartOptions'][$k.'_inside'] ];
        }
      }
    }

    // reorder data by index (to match categories)
    ksort($this->data);

    // columns keys
    $colkeys = array_keys($fulldata);

    

    // compute percentage using different column
    $percentTotal = false;
    if (!empty($this->config['dotStatChartOptions']['percentTotal'])) {
      $percentTotal = $this->config['dotStatChartOptions']['percentTotal'];
    }

    // rawdata
    foreach ($this->data as $ks => $arr) {
      // series (groups)
      $colchk = [];
      // calc percentage
      $total = 0;
      if ($percentTotal) {
        $percentDim = array_key_exists($ks, $percentTotal)?$percentTotal[$ks]:$percentTotal['default'];
        if (is_numeric($percentDim)) {
          $total = $percentDim;
        } else if (array_key_exists($percentDim, $arr)) {
          $total = $arr[$percentDim]['value'];
        }
      }

      if (!empty($colselection)) {
        foreach ($colselection as $ki) {
          // error_log(' > '.$ki.' = '.$arr[$ki]['value']);
          $colchk[] = $ki;
          $this->_loopValue($ki, $arr[$ki], $total, $fulldata);
        }
      } else {
        foreach ($arr as $ki => $arv) {
          // error_log(' > '.$ki.' = '.$arv['value']);
          $colchk[] = $ki;
          $this->_loopValue($ki, $arv, $total, $fulldata);
        }
        // complete missing column values
        if (count($colkeys) != count($colchk)) {
          $coldiff = array_diff($colkeys, $colchk);
          foreach ($coldiff as $kc) {
            $fulldata[$kc]['data'][] = '';
          }
        }
      }

    }

    // order by values
    $ordered = [];
    foreach ($fulldata as $k => $arr) {
      $total = array_sum($arr['data']);
      $ordered[$total] = $arr;
    }

    krsort($ordered);

    return array_values($ordered);
  }

  protected function _loopValue($ki, $arv, $total, &$fulldata) {

    foreach ($arv as $kv => $vv) {
        // data
      if ($kv == 'value') {
        if ($total) {
          $vv = min(round($vv * 100 / $total), 100);
        }
        // array of values
        $fulldata[$ki]['data'][] = $vv;
      } else {
        // other keys = last value only
        $fulldata[$ki][$kv] = $vv;
      }
    }

  }

	public function getHighchartsOptions() {
		return <<<EOS
{
  "chart": {
    "type": "column",
    "backgroundColor": "#fff",
    "height": 400
  },
  "xAxis": {
    "type": "category"
  },
  "yAxis": {
    "title": { "text": "" }
  },
  "legend": {
    "enabled": true
  },
  "plotOptions": {
    "series": {
      "pointPadding": 0.1,
      "groupPadding": 0.1,
      "borderWidth": 1,
      "maxPointWidth": 70,
      "dataLabels": {
        "enabled": false
      }
    }
  },
  "credits": {
    "enabled": false
  },
  "dotStatChartOptions": {
    "rawDataType": "columns"
  }
}
EOS;

	}

}