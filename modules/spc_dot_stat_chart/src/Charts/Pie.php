<?php

/**
 * Pie/Donut
 */

namespace Drupal\spc_dot_stat_chart\Charts;

class Pie extends \Drupal\spc_dot_stat_chart\DotStatChartTemplate {

  public function __construct($series, $data, $depth) {
    parent::__construct($series, $data, $depth);
  }

	public function getHighchartsOptions() {
		return <<<EOS
{
  "chart": {
    "type": "pie",
    "backgroundColor": "#fff",
    "height": 300
  },
  "tooltip": {
    "pointFormat": "{series.name}: <b>{point.formatted}</b>"
  },
  "plotOptions": {
    "pie": {
      "allowPointSelect": true,
      "cursor": "pointer",
      "dataLabels": {
        "enabled": false
      },
      "showInLegend": true
    }
  },
  "legend": {
    "enabled": false
  },
  "credits": {
    "enabled": false
  }
}
EOS;

	}

}

