<?php

/**
 * Lollipop charts
 */

namespace Drupal\spc_dot_stat_chart\Charts;

class Lollipop extends \Drupal\spc_dot_stat_chart\DotStatChartTemplate {

  public function __construct($series, $data, $depth) {
    parent::__construct($series, $data, $depth);
  }

  /**
   * Get Highchart formatted dataset from harvested data saved in Drupal entity
   *
   * @return array of data
   */
  public function getData() {
    $rawdata = [];
    switch ($this->depth) {
    case 0:
      // --------------- NO SERIES
      foreach ($this->data as $key => $arv) {
        $rawdata[] = $this->_json2data($key, $arv);
      }
      break;
    case 1:
      // --------------- 1-LEVEL SERIES
      $s1 = reset($this->series);
      foreach($s1['values'] as $sdef) {
        $data = [];
        $sid = $sdef['id'];
        foreach ($this->data[ $sid ] as $key => $arv) {
          // overwrite to keep only last value
          $data = $this->_json2data($key, $arv);
        }
        if (empty($data['y'])) {
        	$data['y'] = '0';
        }
        $data['name'] = $sid;
        $data['label'] = $sdef['name'];
        $rawdata[$sid] = $data;
      }
      ksort($rawdata);
      $rawdata = array_values($rawdata);
    }

    // return array of objects of data
    $rawobj = (object) [
      'data' => $rawdata
    ];
    return [ $rawobj ];
  }

  /**
	 * Adds specific highcharts modules
	 */
	public function getHighchartsModules() {
		return [
			'spc_dot_stat_chart/highcharts-more',
			'spc_dot_stat_chart/highcharts-lollipop'
		];
	}


	public function getHighchartsOptions() {
		return <<<EOS
{
  "chart": {
    "type": "lollipop",
    "backgroundColor": "#fff",
    "height": 300
  },
	"xAxis": {
    "type": "category"
  },
  "yAxis": {
  	"title": { "text": "" }
  },
  "legend": {
    "enabled": false
  },
  "plotOptions": {
    "series": {
    	"connectorWidth": 1,
      "marker": {
        "radius": 6
      },
      "dataLabels": {
        "enabled": false
      }
    }
  },
  "credits": {
    "enabled": false
  },
  "dotStatChartOptions": {
    "rawDataType": "lollipop"
  }
}
EOS;

	}

}