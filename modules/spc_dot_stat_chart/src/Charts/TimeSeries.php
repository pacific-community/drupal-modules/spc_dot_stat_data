<?php

/**
 * Line chart for time series
 */

namespace Drupal\spc_dot_stat_chart\Charts;


class TimeSeries extends \Drupal\spc_dot_stat_chart\DotStatChartTemplate {

  public function __construct($series, $data, $depth) {
    parent::__construct($series, $data, $depth);
  }

  public function getCategories() {
    return NULL;
  }

  public function getData() {

    $rawdata = [];
    
    $s1 = reset($this->series);
    foreach($s1['values'] as $sdef) {
      $data = [];
      $sid = $sdef['id'];
      foreach ($this->data[ $sid ] as $key => $arv) {
        $data[] = $this->_json2data($sdef['name'], $arv, ['year' => 'x', 'date' => 'x']);
      }
      $rawdata[$sid] = $data;
      ksort($rawdata);
    }
    
    return $rawdata;
  }

	public function getHighchartsOptions() {
		return <<<EOS
{
  "chart": {
    "type": "line",
    "backgroundColor": "#fff",
    "height": 300
  },
  "xAxis": {
    "type": "datetime"
  },
  "legend": {
    "align": "right",
    "floating": false,
    "enabled": true,
    "layout": "vertical"
  },
  "yAxis": {
    "title": { "text": "" }
  },
  "credits": {
    "enabled": false
  },
  "dotStatChartOptions": {
    "rawDataType": "time_series",
    "colors": {
      "CK": "#08294b",
      "FJ": "#1aadce",
      "FM": "#77a1e5",
      "KI": "#c42525",
      "PF": "#0757b3",
      "MH": "#dc931f",
      "NC": "#0757b3",
      "NR": "#e7904e",
      "NU": "#ecda11",
      "PG": "#910000",
      "PW": "#1aadce",
      "SB": "#94ca20",
      "TO": "#c42525",
      "TV": "#1ace99",
      "VU": "#3aa411",
      "WS": "#492970"
    },
    "symbol": {
      "CK": "triangle",
      "FJ": "circle",
      "FM": "square",
      "KI": "diamond",
      "PF": "triangle",
      "MH": "triangle",
      "NC": "circle",
      "NR": "diamond",
      "NU": "square",
      "PG": "triangle-down",
      "PW": "circle",
      "SB": "triangle-down",
      "TO": "square",
      "TV": "diamond",
      "VU": "triangle-down",
      "WS": "diamond"
    }
  },
  "responsive": {
    "rules": [{
      "condition": {
        "maxWidth": 768
      },
      "chartOptions": {
        "chart": {
          "height": 520
        },
        "legend": {
          "align": "center",
          "verticalAlign": "bottom",
          "layout": "horizontal"
        }
      }
    }]
  }
}
EOS;

	}

  /**
   * Get Javascript class name
   */
  public function getAdditionalLibraries() {
    return [ 
      'spc_dot_stat_chart/statchart-time-series'
    ];
  }

  /**
   * Get Javascript class name
   */
  public function getJavascriptClass() {
    return 'DotStatChartTimeSeries';
  }

}

