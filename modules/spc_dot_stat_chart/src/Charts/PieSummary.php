<?php

/**
 * Pie/Donut
 * Specific to SDG in the Pacific dashboard
 * NO NEED TO BE PORTED TO WP/Joomla
 */

namespace Drupal\spc_dot_stat_chart\Charts;

class PieSummary extends \Drupal\spc_dot_stat_chart\DotStatChartTemplate {

  public function __construct($series, $data, $depth) {
    parent::__construct($series, $data, $depth);
  }

  public function getCategories() {
    return [
      'Yes',
      'No',
      'No answer'
    ];
  }

  public function getData() {

    $data = [
      'Yes' => [
        'name' => 'Yes',
        'label' => '',
        'color' => '#6c6',
        'y' => 0
      ],
      'No' => [
        'name' => 'No',
        'label' => '',
        'color' => '#f66',
        'y' => 0
      ],
      'No answer' => [
        'name' => 'No answer',
        'label' => '',
        'color' => '#aaa',
        'y' => 0
      ]
    ];

    $countries = [
      // 'AS' => 'American Samoa', 
      'CK' => 'Cook Islands',
      'FJ' => 'Fiji',
      'FM' => 'Micronesia (Federated States of)',
      // 'GU' => 'Guam',
      'KI' => 'Kiribati',
      'MH' => 'Marshall Islands',
      // 'MP' => 'Northern Mariana Islands',
      'NC' => 'New Caledonia',
      'NR' => 'Nauru',
      'NU' => 'Niue',
      'PF' => 'French Polynesia',
      'PG' => 'Papua New Guinea',
      // 'PN' => 'Pitcairn Islands',
      'PW' => 'Palau',
      'SB' => 'Solomon Islands',
      // 'TK' => 'Tokelau',
      'TO' => 'Tonga',
      'TV' => 'Tuvalu',
      'VU' => 'Vanuatu',
      // 'WF' => 'Wallis and Futuna',
      'WS' => 'Samoa'
    ];

    switch ($this->depth) {
    case 0:
      // --------------- NO SERIES
      // @todo
      break;
    case 1:
      // --------------- 1-LEVEL SERIES
      $s1 = reset($this->series);
      foreach($s1['values'] as $sdef) {
        $sid = $sdef['id'];
        // take last one only
        $last = array_pop($this->data[$sid]);
        if (array_key_exists($sid, $countries)) {
          switch ($last['value']) {
            case '0':
              $data['No']['label'] .= '<br />- '.$countries[$sid];
              $data['No']['y'] += 1;
              break;
            case '1':
            case '100':
              $data['Yes']['label'] .= '<br />- '.$countries[$sid];
              $data['Yes']['y'] += 1;
              break;
          }
        }
        unset($countries[$sid]);
      }

      // count missing ones
      foreach ($countries as $cid => $name) {
        $data['No answer']['label'] .= '<br />- '.$name;
        $data['No answer']['y'] += 1;
      }

      // return values
      $data = array_values($data);
    }

    // return array of objects of data
    $rawobj = (object) [
      'data' => $data
    ];
    return [ $rawobj ];
  }

	public function getHighchartsOptions() {
		return <<<EOS
{
  "chart": {
    "type": "pie",
    "backgroundColor": "#fff",
    "height": 300
  },
  "tooltip": {
    "headerFormat": "",
    "pointFormat": "<b>{point.name}<b>: {point.percentage:.1f}%<br /><em>Countries</em>{point.label}",
    "outside": true
  },
  "plotOptions": {
    "pie": {
      "allowPointSelect": true,
      "cursor": "pointer",
      "dataLabels": {
        "enabled": false
      },
      "showInLegend": true
    }
  },
  "legend": {
    "enabled": true,
    "layout": "vertical",
    "align": "right",
    "verticalAlign": "middle"
  },
  "credits": {
    "enabled": false
  }
}
EOS;

	}

}

