<?php

/**
 * Column chart with Drilldown (as line chart)
 */

namespace Drupal\spc_dot_stat_chart\Charts;

class Drilldown extends \Drupal\spc_dot_stat_chart\DotStatChartTemplate {

  public function __construct($series, $data, $depth) {
    parent::__construct($series, $data, $depth);
  }

  /**
   * Get Highchart formatted dataset from harvested data saved in Drupal entity
   *
   * @return array of data
   */
  public function getData() {
    $rawdata = [];
    switch ($this->depth) {
    case 0:
      // --------------- NO SERIES
      // no drilldown when no series
      break;
    case 1:
      // --------------- 1-LEVEL SERIES
      $s1 = reset($this->series);
      foreach($s1['values'] as $sdef) {
        $data = [];
        $sid = $sdef['id'];
        foreach ($this->data[ $sid ] as $key => $arv) {
          // overwrite to keep only last value
          $data = $this->_json2data($key, $arv);
        }
        $data['name'] = $sdef['name'];
        $data['drilldown'] = $sid;
        $rawdata[$sid] = $data;
      }
      ksort($rawdata);
      $rawdata = array_values($rawdata);
    }

    // return array of objects of data
    return $rawdata;
  }

  public function getDrillData() {

    $series = [];
    $level1 = reset($this->series);
    foreach ($level1['values'] as $obj) {
      $series[ $obj['id']] = $obj['name'];
    }

    $drills = [];

    foreach ($this->data as $drill => $dset) {
      $data = [];
      foreach ($dset as $yid => $drdata) {
        if (!empty($data) && !empty($drdata['year']) && $yid != $drdata['year']) {
          // skip non reference years
          continue;
        }
        $data[] = [ "$yid", $drdata['value'] ];
      }
      $drills[] = (object) [
        'xAxis' => 1,
        'id' =>  $drill,
        'name' => $series[$drill],
        'type' => 'line', // @todo drilldown chart type should be selectable (actually fully configurable)
        'getExtremesFromAll' => true,
        'tooltip' => [
          'headerFormat' => '<em>{series.name}<br />',
          'pointFormat' => '{point.y} in {point.x}'
        ],
        'data' => $data
      ];
    }

    return $drills;
    
  }

  public function getHighchartsModules() {
    return [ 'spc_dot_stat_chart/highcharts-drilldown' ];
  }

	public function getHighchartsOptions() {
		return <<<EOS
{
  "chart": {
    "type": "column",
    "backgroundColor": "#fff",
    "height": 300
  },
  "xAxis": [{
    "type": "category"
  }, {
    "type": "datetime"
  }],
  "yAxis": {
    "title": { "text": "" }
  },
  "legend": {
    "enabled": false
  },
  "plotOptions": {
    "series": {
      "pointPadding": 0,
      "groupPadding": 0,
      "borderWidth": 1,
      "dataLabels": {
        "enabled": false
      }
    }
  },
  "credits": {
    "enabled": false
  },
  "dotStatChartOptions": {
    "rawDataType": "drilldown"
  },
  "drilldown": {
     "drillUpButton": {
        "relativeTo": "spacingBox",
        "position": {
          "align": "left",
          "verticalAlign": "top",
          "x": 30,
          "y": 0
       }
     }
  },
  "responsive": {
     "rules": [{
      "condition": {
        "maxWidth": 768
      },
      "chartOptions": {
        "drilldown": {
          "drillUpButton": {
            "relativeTo": "plotBox",
            "position": {
              "align": "left",
              "verticalAlign": "top",
              "x": 0,
              "y": -20
            }
          }
        }
      }
    }]
  }
}
EOS;

	}

  /**
   * Get Javascript class name
   */
  public function getAdditionalLibraries() {
    return [ 
      'spc_dot_stat_chart/statchart-drilldown'
    ];
  }

  /**
   * Get Javascript class name
   */
  public function getJavascriptClass() {
    return 'DotStatChartDrilldown';
  }

}

