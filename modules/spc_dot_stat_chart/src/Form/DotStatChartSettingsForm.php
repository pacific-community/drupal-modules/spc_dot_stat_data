<?php

namespace Drupal\spc_dot_stat_chart\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DotStatDataSettingsForm.
 *
 * @ingroup spc_dot_stat_data
 */
class DotStatChartSettingsForm extends ConfigFormBase {

  const SETTINGS = 'spc_dot_stat_chart.settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'spc_dot_stat_chart_settings';
  }

  protected function getTemplateList() {
    // get list of templates
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('spc_dot_stat_chart')->getPath().'/src/Charts';

    $arf = \Drupal::service('file_system')->scanDirectory($module_path, '/\.php$/', [ 'key'=>'filename' ]);
    $art = array();
    foreach ($arf as $fn => $ff) {
      $fn = substr($fn, 0, strrpos($fn, '.'));
      $fs = preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", $fn);
      $ff = strtolower(str_replace(' ','_', $fs));
      $art [ $fn ] = $fs;
    }

    return $art;

  }

  /**
   * Defines the settings form for SPC .Stat Data entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    // Default settings.
    $config = $this->config(static::SETTINGS);

    // prepare tabs
    $form['templates'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-columns'
    ];

    $art = $this->getTemplateList();

    foreach ($art as $fn => $fs) {

      $ff = strtolower(str_replace(' ','_', $fs));

      $form[$ff] = [
        '#type' => 'details',
        '#title'=> $fs,
        '#group' => 'templates'
      ];

      $value = trim($config->get($ff));
      if (empty($value)) {
        // get default value from template class
        $tplclass = "Drupal\\spc_dot_stat_chart\\Charts\\{$fn}";

        $objTemplate = new $tplclass(null, null, null);

        $value = $objTemplate->getHighchartsOptions();
      }

      $form[$ff]['tpl_'.$ff] = [
        '#type' => 'textarea',
        '#title' => $fs.' Configuration',
        '#default_value' => $value,
        '#rows' => 20
      ];

    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $art = $this->getTemplateList();

    foreach ($art as $fn => $fs) {

      $ff = strtolower(str_replace(' ','_', $fs));

      $value = trim($form_state->getValue('tpl_'.$ff));

      if (strlen($value) > 0) {

        $value = preg_replace('/function\(\) {[^}]+}/','""', $value);

        $testjson = json_decode($value, true);
        if (is_null($testjson)) {
          $form_state->setErrorByName('tpl_'.$ff, $this->t('JSON format invalid ('.json_last_error_msg().')'));
        }
      }
    }
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
    $conf = $this->configFactory->getEditable(static::SETTINGS);

    $art = $this->getTemplateList();

    foreach ($art as $fn => $fs) {

      $ff = strtolower(str_replace(' ','_', $fs));

      $value = $form_state->getValue('tpl_'.$ff);

      if (empty($value)) {
        // get default value from template class
        $tplclass = "Drupal\\spc_dot_stat_chart\\Charts\\{$fn}";

        $objTemplate = new $tplclass(null, null, null);

        $value = $objTemplate->getHighchartsOptions();
      }

      $conf->set($ff, $value);
    }
    
    $conf->save();
    
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS
    ];
  }

}
