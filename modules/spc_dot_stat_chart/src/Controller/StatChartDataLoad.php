<?php
namespace Drupal\spc_dot_stat_chart\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
// use Symfony\Component\HttpFoundation\JsonResponse;

class StatChartDataLoad extends ControllerBase
{
  // $variable is the wildcard from the route
  public function ajaxCallback($nodeid, $template)
  {
    
    $entity_storage = \Drupal::entityTypeManager()->getStorage('dot_stat_data');

    $node = $entity_storage->load($nodeid);

    if (!$node) {
    	return  new JsonResponse(['status' => -1, 'error' => "Cannot load node #".$nodeid ]);
    }

    $tpl = str_replace('_', '', ucwords($template, '_'));
    
    // check class is defined
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('spc_dot_stat_chart')->getPath();
    if (!file_exists($module_path.'/src/Charts/'.$tpl.'.php')) {
      return  new JsonResponse(['status' => -1, 'error' => "$tpl does not exists" ]);
    }
    
    $tplclass = "Drupal\\spc_dot_stat_chart\\Charts\\{$tpl}";

    $objTemplate = new $tplclass(
      $node->getSeriesJson(),
      $node->getKeyData(),
      $node->getDataDepth()
    );
    
    // --------------- PARSE CATEGORIES AND DATA

    $rawdata = $objTemplate->getData();

		$ars = [
			'status' => 0,
			'label'  => $node->label(),
			'data'	 => $rawdata
		];
	  
	  return new AjaxResponse($ars);
  }
}