<?php

namespace Drupal\spc_dot_stat_chart;

use Drupal\Core\Datetime\DrupalDateTime;

abstract class DotStatChartTemplate {

	protected $series;
	protected $data;
	protected $depth;
	protected $config;

	public function __construct($series, $data, $depth) {
		$this->series = $series;
		$this->data = $data;
		$this->depth = $depth;
	}

	/**
	 * Set options from module config
	 */
	public function setOptions($jdata) {
		$this->config = $jdata;
	}

	/**
	 * Get Highchart formatted categories from either data or series 
	 * 
	 * @return mixed array of categories or string setting mode
	 */
	public function getCategories() {
		switch ($this->depth) {
		case 0:
			// --------------- NO SERIES
			// get key from data index
			$categories = array_keys($this->data);
			break;
		default:
			// --------------- DATA SERIES
			// get keys from dimensions definitions
			// use dimension ID by default (not name)
			$level1 = reset($this->series);
			foreach ($level1['values'] as $obj) {
				$categories[] = $obj['id'];
			}
		break;
		}
		sort($categories);
		return $categories;
	}

	/**
	 * Parse JSON data to render data for Highcharts
	 *
	 * @param $name name of data serie
	 * @param $obj JSON object holding data
	 * @param $map map object properties to rendered properties
	 *
	 * @return array representing a single row of data
	 */
	protected function _json2data($name, $obj, $map='') {
		// check file map
		if (!$map) {
			$map = [];
		}
		// get name from index
		$data = [
			'name' => $name
		];
		// get value
		$val = 'n/c';
		foreach ($obj as $k => $v) {
			$kt = $k;
			if (array_key_exists($k, $map)) {
				$kt = $map[$k];
			} else {
				switch($k) {
				case 'value':
					// get value
					$kt = 'y';
					// save for formatted value
					$val = $v;
					break;
				}
			}
			if($kt == "x" and $k == "year"){
				# if year provided, convert it to Datetime object
				$data[$kt] = \DateTime::createFromFormat("Y", $v);
			} else if($kt == "x" and $k == "date"){
				# if date is provided, try to convert it to Datetime object
				if(count(explode("-", $v)) == 3){
					$data[$kt] = \DateTime::createFromFormat("Y-m-d", $v);
				} else if(count(explode("-", $v)) == 2){
					$data[$kt] = \DateTime::createFromFormat("Y-m", $v);
				} else {
					$data[$kt] = \DateTime::createFromFormat("Y", $v);
				}
				# we need 'date' to be used in subtitle
				$data[$k] = $v;
			} else if (!empty($v) || $v === 0) {
				// do not set value if data is missing (no value)
				$data[$kt] = $v;
			}
		}

		if (is_numeric($val)) {
			// format value (y)
			$data['formatted'] = number_format($val);
		}
		return $data;
	}

  /**
   * Get Highchart formatted dataset from harvested data saved in Drupal entity
   *
   * @return array of data
   */
	public function getData() {
		$rawdata = [];
		switch ($this->depth) {
    case 0:
    	// --------------- NO SERIES
			foreach ($this->data as $key => $arv) {
				$rawdata[] = $this->_json2data($key, $arv);
      }
      break;
    case 1:
    	// --------------- 1-LEVEL SERIES
    	$s1 = reset($this->series);
	    foreach($s1['values'] as $sdef) {
	      $data = [];
	      $sid = $sdef['id'];
	      foreach ($this->data[ $sid ] as $key => $arv) {
	        $data[] = $this->_json2data($key, $arv);
	      }
	      $rawdata[$sid] = $data;
    	}
    }

    // return array of objects of data
    $rawobj = (object) [
      'data' => $rawdata
    ];
    return [ $rawobj ];
	}

	/**
	 * Get Highchart formatted data for drilldown
	 *
	 * @return array of data or false for no drilldown
	 */
	public function getDrillData() {
		return false;
	}

	/**
	 * Adds specific highcharts modules
	 */
	public function getHighchartsModules() {
		return [];
	}

	/**
	 * Get Javascript class name
	 */
	public function getAdditionalLibraries() {
		return [];
	}

	/**
	 * Get Javascript class name
	 */
	public function getJavascriptClass() {
		return 'DotStatChart';
	}

	/**
	 * Defines Javascript options
	 *
	 * @return string representing Javascript object (not JSON)
	 */
	abstract public function getHighchartsOptions();
}

