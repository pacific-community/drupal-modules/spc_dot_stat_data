<?php

namespace Drupal\spc_dot_stat_dataflows;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\node\Entity\Node;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;

/**
 * Synchronises categories and dataflows from .stat.
 */
class DotStatDataflowSync {

	protected $database;
	protected $entityTypeManager;
	protected $languageManager;
	protected $service;

	/**
   * DotStatDataflowSync constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $language_manager) {
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $language_manager;

    /** @var \Drupal\spc_dot_stat_data\DotStatDataRequest $this->service */
    $this->service = \Drupal::service('spc_dot_stat_data.request');
  }

  /**
   * Gets the current settings for the service.
   *
   * @return array
   *   An array of settings.
   */
  protected function getSettings() {
    $config = \Drupal::config('spc_dot_stat_dataflows.settings');
    return [
      'datasource'	      => $config->get('dot_stat_datasource'),
      'dot_stat_nsi'      => $config->get('dot_stat_nsi'),
      'dot_stat_explorer' => $config->get('dot_stat_explorer')
    ];
  }

  protected function getLangOptions() {
  	$langs = [];
  	$langDefault = $this->languageManager->getDefaultLanguage()->getId();
  	$langs['default'] = $langDefault;
  	$langs['all'] = [ $langDefault ];
  	$langs['translations'] = [];
  	foreach ($this->languageManager->getLanguages(LanguageInterface::STATE_CONFIGURABLE) as $lang) {
  		$lgcode = $lang->getId();
  		if (!in_array($lgcode, $langs['all'])) {
	  		$langs['all'][] = $lgcode;
	  		$langs['translations'][] = $lgcode;
	  	}
  	}
  	return $langs;
  }

  public function syncCategories() {

  	// counters
  	$counterSkip = $counterNew = $counterDelete = 0;

  	// get settings
  	$config = $this->getSettings(); 

  	// get languages conf
  	$confLanguage = $this->getLangOptions();
  	
  	// load existing terms
  	$exTermsList = [];
  	$query = \Drupal::entityQuery('taxonomy_term');
		$query->condition('vid', 'dot_stat_categories');
		$tids = $query->execute();
		$terms = \Drupal\taxonomy\Entity\Term::loadMultiple($tids);
		foreach ($terms as $term) {
		    $exTermsList[ $term->field_category_code->value ] = $term;
		}

    // prepare query
    $qry = $config['dot_stat_nsi'].'/rest/categoryscheme/SPC/all/latest?detail=full';

    // query XML
    $xml = $this->service->queryXml($qry);

    if (empty($xml)) {
      // no result > stop here
      \Drupal::logger('spc_dot_stat_dataflows')->warning('.Stat links: categoryscheme request failed (no result)<br /><code>'.$qry.'</code>');
      return;
    }

    // create XML object
    $xroot = new \SimpleXMLElement($xml);

    // parse XML for Category Schemes
    $xmlCategorySchemes = $xroot->xpath("/message:Structure/message:Structures/structure:CategorySchemes/structure:CategoryScheme");
    
    // loop through category schemes
    foreach ($xmlCategorySchemes as $xmlCatScheme) {
      $attrs = $xmlCatScheme->attributes();
      $sid 			= (string) $attrs['id'];
      $agencyId = (string) $attrs['agencyID'];
      $version	= (string) $attrs['version'];
      $name = [];
      foreach ($confLanguage['all'] as $lg) {
      	$name[ $lg ] = $xmlCatScheme->xpath('common:Name[@xml:lang="'.$lg.'"]')[0];
      }

      // create taxonomy (if does not exist)
      $parentId = 0;
      if (array_key_exists($sid, $exTermsList)) {
      	// already exists
      	$parentId = $exTermsList[$sid]->id();
      	unset($exTermsList[$sid]);
				$counterSkip++;
			} else {
	      $new_term = Term::create([
			    'name' => $name[ $confLanguage['default'] ],
			    'description' => '',
			    'field_category_code' => $sid,
			    'vid' => 'dot_stat_categories',
			    'parent' => null
			  ]);
				$new_term->save();
				$parentId = $new_term->id();

				// add translation
				foreach ($confLanguage['translations'] as $lg) {
					$trans_term = $new_term->addTranslation($lg, [
						'name' => $name[ $lg ],
				    'description' => '',
				    'field_category_code' => $sid,
				    'vid' => 'dot_stat_categories',
				    'parent' => null
					]);
					$trans_term->save();
				}

				$counterNew++;
			}

      // parse XML for Categories
      $xmlCategories = $xmlCatScheme->xpath("structure:Category");

      // loop through categories
      foreach ($xmlCategories as $xmlCat) {
      	$attrs = $xmlCat->attributes();
	      $cid = (string) $attrs['id'];
	      $name = [];
	      foreach ($confLanguage['all'] as $lg) {
	      	$name[ $lg ] = $xmlCat->xpath('common:Name[@xml:lang="'.$lg.'"]')[0];
	      }

	      // create taxonomy (if does not exist)
      	$childId = 0;
	      if (array_key_exists($cid, $exTermsList)) {
	      	// already exists
	      	$childId = $exTermsList[$cid]->id();
	      	unset($exTermsList[$cid]);
					$counterSkip++;
				} else {
	      	$new_term = Term::create([
				    'name' => $name[ $confLanguage['default'] ],
				    'description' => '',
				    'field_category_code' => $cid,
				    'vid' => 'dot_stat_categories',
				    'parent' => $parentId
				  ]);
					$new_term->save();
					$childId = $new_term->id();

					// add translation
					foreach ($confLanguage['translations'] as $lg) {
						$trans_term = $new_term->addTranslation($lg, [
							'name' => $name[ $lg ],
					    'description' => '',
					    'field_category_code' => $cid,
					    'vid' => 'dot_stat_categories',
					    'parent' => $parentId
						]);
						$trans_term->save();
					}

					$counterNew++;
	      }
      }
		}

		// delete old ones
    foreach ($exTermsList as $code => $term) {
    	$term->delete();
    	$counterDelete++;
    }

	  \Drupal::logger('spc_dot_stat_dataflows')->info(
	  	"Finished .Stat category sync (@newly created, @deleted deleted, @skipped skipped)", [
		    '@newly' => $counterNew,
		    '@deleted' => $counterDelete,
		    '@skipped' => $counterSkip
		  ]
	  );

	  \Drupal::messenger()->addStatus(t(
	  	"Finished .Stat category sync (@newly created, @deleted deleted, @skipped skipped)", [
		    '@newly' => $counterNew,
		    '@deleted' => $counterDelete,
		    '@skipped' => $counterSkip
		  ]
		));
	}

	public function syncDataflows() {

		// error_log('============');
		// error_log('= NEW SYNC =');
		// error_log('============');

		// DEVLPT LOOP LIMIT
		$limit = 0;

		// Country constraint
		$countryKey = 'GEO_PICT';

		// counters
		$counterSkip = $counterNew = $counterDelete = 0;

		// get settings
  	$config = $this->getSettings(); 

  	// get languages conf
  	$confLanguage = $this->getLangOptions();

  	// load existing dataflows
  	$exItemList = [];
  	$nids = $this->entityTypeManager->getStorage('node')
  		->getQuery()
  		->condition('type','dot_stat_dataflow')
  		->execute();
		$items = Node::loadMultiple($nids);
		foreach ($items as $item) {	
			$code = $item->get('field_code')->value;
		  $exItemList[ $code ] = $item;
		}

		// load categories (taxonomy)
		$taxCategoryList = array();
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', 'dot_stat_categories');
    $query->sort('field_category_code');
    $cids = $query->execute();
    $terms = \Drupal\taxonomy\Entity\Term::loadMultiple($cids);
    foreach($terms as $term) {
      $taxCategoryList[ $term->id() ] = $term->get('field_category_code')->value;
    }

  	// prepare query
    $qry = $config['dot_stat_nsi'].'/rest/dataflow/SPC/all/all';

    // query XML
    $xmlAll = $this->service->queryXml($qry);

    if (empty($xmlAll)) {
      // no result > stop here
      \Drupal::logger('spc_dot_stat_dataflows')->warning('.Stat links: dataflow (all) request failed (no result)<br /><code>'.$qry.'</code>');
      return;
    }

    // create XML object
    $xroot = new \SimpleXMLElement($xmlAll);

    // parse XML for Dataflows
    $xmlDataflows = $xroot->xpath("/message:Structure/message:Structures/structure:Dataflows/structure:Dataflow");
    
    // loop through dataflows
    foreach ($xmlDataflows as $xmlItem) {
      $attrs = $xmlItem->attributes();
      $fid  = (string) $attrs['id'];

      if (array_key_exists($fid, $exItemList)) {
      	// skip existing
      	$counterSkip++;
      	// remove from list of existing stuff (to avoid deletion)
      	unset($exItemList[$fid]);
      	continue;
      }

      // BASIC DATAFLOW PROPERTIES

      $agencyId = (string) $attrs['agencyID'];
      $version	= (string) $attrs['version'];

      // -- Prepare translatable fields
      $name = [];
      $desc = [];
      $extr = [];

      foreach ($confLanguage['all'] as $lg) {
      	$name[ $lg ] = $xmlItem->xpath('common:Name[@xml:lang="'.$lg.'"]')[0];
      	$desc[ $lg ] = $xmlItem->xpath('common:Description[@xml:lang="'.$lg.'"]')?
      		$xmlItem->xpath('common:Description[@xml:lang="'.$lg.'"]')[0]:'';
      	$extr[ $lg ] = '';
      }

      // error_log('------------');
      // error_log($name['en']);
      // error_log($name['fr']);
      // error_log('------------');      

		  // MORE DATAFLOW PROPERTIES

		  $qrydf = $config['dot_stat_nsi'].'/rest/dataflow/SPC/'.$fid.'/all?references=all';
	    $xmlFlow = $this->service->queryXml($qrydf);

	    // error_log('QRY: '.$qrydf);

	    if (!$xmlFlow) {
	    	// error_log('/!\\ REST API query error');
	    	continue;
	    }

	    $xflow = new \SimpleXMLElement($xmlFlow);

	    // --- External resources

	    $xmlResources = $xflow->xpath(
	    	'/message:Structure/message:Structures/structure:Dataflows'
	    	.'/structure:Dataflow[@id="'.$fid.'"]'
	    	.'/common:Annotations/common:Annotation[common:AnnotationType="EXT_RESOURCE"]'
	  	);
	  	if ($xmlResources) {
	  		foreach ($confLanguage['all'] as $lg) {
	        $txt = $xmlResources[0]->xpath('common:AnnotationText[@xml:lang="'.$lg.'"]'); // @todo translate
	        if (!empty($txt)) {
	        	$extr[ $lg ] = (string) $txt[0];
	        }
	      }
	    }

	    // --- define properties with default language values

	    $data = [
      	'type'          => 'dot_stat_dataflow',
      	'title'         => $name[ $confLanguage['default'] ],
      	'path'					=> '/dataflow/'.strtolower(str_replace(' ','_',$fid)),
      	'langcode'      => $confLanguage['default'],
      	'field_code'    => $fid,
		    'field_description'   => $desc[ $confLanguage['default'] ],
		    'field_parent_code'   => '',
		    'field_datasource_id' => $config['datasource'],
		    'field_agency_id'     => $agencyId,
		    'field_sdmx_version'  => $version,
		    'field_constraints'   => '',
		    'field_external_resources' => $extr[ $confLanguage['default'] ]
		  ];

	    // --- parse XML for categorisation

	    $xmlCategories = $xflow->xpath("/message:Structure/message:Structures/structure:Categorisations/structure:Categorisation");
	    $arrCats = [];
	    // error_log('Categories for '.$fid);
	    foreach ($xmlCategories as $xmlCat) {
	      $cid = (string) $xmlCat->xpath('structure:Target/Ref')[0]->attributes()['id'];
	      if ($ccode = array_search($cid, $taxCategoryList)) {
	      	// error_log('> '.$ccode);
	      	$arrCats[] = $ccode;
	      }
	    }
	    $data['field_categories'] = $arrCats;
		  
	    // -- parse XML for query constraints

	    $arrCountries = [];

	    $arrConstraints = [];

	    /// multilanguage
	    foreach ($confLanguage['all'] as $lg) {

	    	$arrConstraints[ $lg ] = [];
	    
		    $xq = '/message:Structure/message:Structures'
	        .'/structure:Constraints/structure:ContentConstraint[@type="Actual"]'
	        .'/structure:CubeRegion/common:KeyValue';

	      $xmlConstraintList = $xflow->xpath($xq);

	      foreach ($xmlConstraintList as $xmlConstraintItem) {

	        $attrs = $xmlConstraintItem->attributes();

	        $conkey = (string) $attrs['id'];
	        
	        if ($conkey == 'TIME_PERIOD') {
	        	// skip time period constraint
	        	// error_log('~> '.$conkey.' (skipped) ');
	          continue;
	        }

	        // --- parse XML for indicators

					$xmlConstraintCheck = $xmlConstraintItem->xpath('common:Value');

					if (empty($xmlConstraintCheck)) {
						// error_log('!> '.$conkey.' (no values)');
						continue;
					}

					// codelist info

					$xpath = '/message:Structure/message:Structures'
						.'/structure:Concepts/structure:ConceptScheme'
						.'/structure:Concept[@id="'.$conkey.'"]'
						.'/common:Name[@xml:lang="'.$lg.'"]'; // @todo translate

					$xmlConcept = $xflow->xpath($xpath);

					$conname = $xmlConcept?((string) $xmlConcept[0]):'';

					// error_log('#> '.$conkey.': '.$conname);

		      // get indicator constraints

		      $xpath = '/message:Structure/message:Structures'
		        .'/structure:DataStructures/structure:DataStructure'
		        .'/structure:DataStructureComponents'
		        .'/structure:DimensionList'
		        .'/structure:Dimension[@id="'.$conkey.'"]'
		        .'/structure:LocalRepresentation/structure:Enumeration/Ref';
	        
	       	$xmlDimEnum = $xflow->xpath($xpath);
	        
	       	if ($xmlDimEnum) {
		        
		        $attrs = $xmlDimEnum[0]->attributes();
		        $dimEnumId = $attrs['id'];

		        // loop through constraint values

		        $arrConstraintValues = [];

		        foreach ($xmlConstraintCheck as $xmlConstItem) {
	        
		          $conid = $concode = (string) $xmlConstItem;

		          $xpath = '/message:Structure/message:Structures'
		            .'/structure:Codelists/structure:Codelist[@id="'.$dimEnumId.'"]'
		            .'/structure:Code[@id="'.$conid.'"]';
		          
		          $xmlCodeItem = $xflow->xpath($xpath);

		          if ($xmlCodeItem) {

		          	// @todo translate
		            $contitle = (string) $xmlCodeItem[0]->xpath('common:Name[@xml:lang="'.$lg.'"]')[0];
		            $dcheck = $xmlCodeItem[0]->xpath('common:Description[@xml:lang="'.$lg.'"]');
		            $condesc = ($dcheck)?(string) $dcheck[0]:'';
		           
		            if (preg_match('/\((\d+\.[\da-z]+\.[\d]+)\)$/', $contitle, $arm)) {
		              $conid = $arm[1];
		            }

		            // Rank
		            $rank = 0;
		            $xmlRank = $xmlCodeItem[0]->xpath('common:Annotations/common:Annotation[common:AnnotationType="ORDER"]');
		            if ($xmlRank) {
		              $rank = $xmlRank[0]->xpath('common:AnnotationText[@xml:lang="'.$lg.'"]'); // @todo translate
		              if (!empty($rank)) {
		              	$rank = (string) $rank[0];
		              	$conid = str_pad($rank, 7, '0', STR_PAD_LEFT).'-'.$conid;
		              }
		            }

		            // error_log('  > '.$conid.': '.$concode.': '.$contitle);

		            $arrConstraintValues[ $conid ] = [
		              'code'  => $concode,
		              'title'  => $contitle,
		              'description' => $condesc
		            ];

		            if (!empty($rank)) {
		            	$arrConstraintValues[ $conid ][ 'rank' ] = $rank;
		            }

		            if ($lg == $confLanguage['default'] && $conkey == $countryKey) {
		            	// add country field (not translatable, do it only once)
		            	$arrCountries[] = $concode;
		            }

	          	} // end if codelist found

	        	} // end loop constraints

	        	ksort($arrConstraintValues);

	        } // end if constraint item exists

	        $arrConstraints[ $lg ][ $conkey ] = [
	        	'name' => $conname,
	        	'values' => array_values($arrConstraintValues)
	        ];

	      } // end loop through constraints

	    }

      $data['field_constraints'] = json_encode($arrConstraints[ $confLanguage['default'] ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

      if (count($arrCountries) > 0) {
      	$data['field_countries'] = $arrCountries;
      }

		  // --- Create item

		  $new_item = Node::create($data);
			$new_item->save();
			$parentId = $new_item->id();

			// --- Add translation

			foreach ($confLanguage['translations'] as $lg) {
				// translate fields
				$data['langcode']      = $lg;
				$data['title']         = $name[ $lg ];
				$data['field_description']   = $desc[ $lg ];
				$data['field_external_resources'] = $extr [ $lg ];
				$data['field_constraints']   = json_encode($arrConstraints[ $lg ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
				// save translation
				$trans_item = $new_item->addTranslation($lg, $data);
				$trans_item->save();
			}

			$counterNew++;

			if ($limit && $counterNew >= $limit) {
				// DEVLPT LOOP BREAK
				break;
			}
		} // end loop

		// delete old ones
		if (!$limit) {
	    foreach ($exItemList as $code => $item) {
	    	$item->delete();
	    	$counterDelete++;
	    }
	  }

	  \Drupal::logger('spc_dot_stat_dataflows')->info(
	  	"Finished .Stat dataflow sync (@newly created, @deleted deleted, @skipped skipped)", [
		    '@newly' => $counterNew,
		    '@deleted' => $counterDelete,
		    '@skipped' => $counterSkip
		  ]
	  );

	  \Drupal::messenger()->addStatus(t(
	  	"Finished .Stat dataflow sync (@newly created, @deleted deleted, @skipped skipped)", [
		    '@newly' => $counterNew,
		    '@deleted' => $counterDelete,
		    '@skipped' => $counterSkip
		  ]
		));

	}
}