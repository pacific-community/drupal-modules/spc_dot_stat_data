<?php

namespace Drupal\spc_dot_stat_dataflows\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManager;

use Drupal\Core\Link;
use Drupal\Core\Url;

use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Displays Key Statistic in a table
 *
 * @Block(
 *   id = "dot_stat_dataflows",
 *   admin_label = @Translation("PDH.stat links to dataflows"),
 *   category = @Translation("SPC .Stat")
 * )
 */
class DotStatDataflows extends BlockBase implements BlockPluginInterface {

  /**
  * {@inheritdoc}
  */
  public function defaultConfiguration() {
    $def = [];
    return $def;
  }

  protected function _getValueFromConfig($class, $value) {
    $defval = null;
    if (!empty($value)) {
      if (is_array($value)) {
        if (count($value) > 1) {
          $defval = $class::loadMultiple($value);
        } else {
          $defval = $class::load($value[0]);
        }
      } else {
        $defval = $class::load($value);
      }
    }
    return $defval;
  }
    
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    // Filter by category
    $form['dot_stat_dataflows_categories'] = array(
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'include_anonymous' => FALSE,
        'target_bundles' => array('dot_stat_categories'),
      ],
      '#title' => t('Filter by category'),
      '#description' => 'Comma separated list of categories.',
      '#size' => 64,
      '#maxlength' => null,
      '#tags' => true,
      '#default_value' => $this->_getValueFromConfig(
        'Drupal\taxonomy\Entity\Term',
        $config['dot_stat_dataflows_categories']
      ),
      '#required' => false
    );

    // Select specific dataflow(s)
    $form['dot_stat_dataflows_dataflows'] = array(
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#selection_settings' => [
        'target_bundles' => [ 'dot_stat_dataflow' ]
      ],
      '#title' => t('Specific dataflow(s)'),
      '#description' => 'Comma separated list of dataflows to display.<br />If already filtered by category, list dataflows to be ignored.',
      '#size' => 64,
      '#maxlength' => null,
      '#tags' => true,
      '#default_value' => $this->_getValueFromConfig(
        'Drupal\node\Entity\Node',
        $config['dot_stat_dataflows_dataflows']
      ),
      '#required' => false
    );

    /*
    // Filter by countries
    $form['dot_stat_dataflows_countries'] = array(
      '#type' => 'textfield',
      '#title' => t('Filter by country'),
      '#description' => 'Comma separated list of 2-letters country codes. Ignored if specific dataflows are defined above.',
      '#size' => 32,
      '#default_value' => $config['dot_stat_dataflows_countries'],
      '#required' => false
    );
    */

    // constraints
    $form['dot_stat_dataflows_constraints'] = array(
      '#type' => 'textfield',
      '#title' => t('Expand dataflow(s) by constraints'),
      '#description' => 'Enter constraint code to show a list of sub-links by constraint e.g. INDICATOR',
      '#size' => 48,
      '#default_value' => $config['dot_stat_dataflows_constraints'],
      '#required' => false
    );

    // expanded?
    $form['dot_stat_dataflows_display'] = array(
      '#type' => 'select',
      '#title' => $this->t('Display mode'),
      '#options' => [
        'collapsed'         => $this->t('Direct links to .stat (collapsed)'),
        'collapsed_nolimit' => $this->t('Direct links to .stat (collapsed, full details)'),
        'expanded'          => $this->t('Direct links to .stat (expanded)'),
        'expanded_nolimit'  => $this->t('Direct links to .stat (expanded, full details)'),
        'entities'          => $this->t('Link to entites')
      ],
      '#default_value' => empty($config['dot_stat_dataflows_display']) ? 'collapsed' : $config['dot_stat_dataflows_display'],
      '#required' => true
    );

    return $form;

  }

  public function blockSubmit($form, FormStateInterface $form_state) {
    
    parent::blockSubmit($form, $form_state);

    // -- SAVE CONFIGURATION -------------------------------
    
    $values = $form_state->getValues();

    foreach ($form_state->getValues() as $key => $value) {

      switch($key) {
        case 'dot_stat_dataflows_categories':
        case 'dot_stat_dataflows_dataflows':
          $this->configuration[ $key ] = [];
          if (!empty($value)) {
            foreach($value as $k => $v) {
              $this->configuration[ $key ][] = $v['target_id'];
            }
          }
          break;
        default:
          $this->configuration[ $key ] = trim($value);
          break;
      }

    }

  }

  public function build() {

    $settings = \Drupal::config('spc_dot_stat_data.settings');

    $config = $this->getConfiguration();

    $html = '';

    // check filter settings
    $dflows = $config['dot_stat_dataflows_dataflows'];
    if (!empty($dflows) && !is_array($dflows)) {
      $dflows = [ $dflows ];
    }

    if (!empty($config['dot_stat_dataflows_categories'])) {
      
      // --- Load by category
      if (count($config['dot_stat_dataflows_categories']) > 1) {
        // multiple categories (separate by category title)
        $html .= '<ul class="dot_stat_categories">';
        foreach ($config['dot_stat_dataflows_categories'] as $catid) {
          // load category
          $category = Term::load($catid);
          if (!$category) {
            continue;
          }
          $html .= '<li><strong>'.$category->getName().'</strong>';
          // prepare query
          $qry = \Drupal::entityTypeManager()->getStorage('node')->getQuery();
          $qry->condition('type','dot_stat_dataflow');
          $qry->condition('field_categories', $catid );
          if (!empty($dflows)) {
            // ignore some dataflows
            $qry->condition('nid', implode(',',$dflows), 'NOT IN');
          }
          // load and display dataflows
          $diz = $qry->execute();
          $nflows = Node::loadMultiple($diz);
          // display list of dataflows
          $html .= $this->_dataflowLoop($nflows, $config['dot_stat_dataflows_constraints'], $config['dot_stat_dataflows_display']);
          $html .= '</li>';
        }
        $html .= '</ul>';
      } else {
        // single category
        // prepare query
        $qry = \Drupal::entityTypeManager()->getStorage('node')->getQuery();
        $qry->condition('type','dot_stat_dataflow');
        $qry->condition('field_categories', array_pop($config['dot_stat_dataflows_categories']) );
        if (!empty($dflows)) {
          // ignore some dataflows
          $qry->condition('nid', $dflows, 'NOT IN');
        }
        // load and display dataflows
        $diz = $qry->execute();
        $nflows = Node::loadMultiple($diz);
        $html .= $this->_dataflowLoop($nflows, $config['dot_stat_dataflows_constraints'], $config['dot_stat_dataflows_display']);
      }

    } else if (!empty($dflows)) {

      // --- Specific dataflows
      $nflows = $this->_getValueFromConfig('Drupal\node\Entity\Node', $dflows);
      $html .= $this->_dataflowLoop($nflows, $config['dot_stat_dataflows_constraints'], $config['dot_stat_dataflows_display']);

    } else {
      // filters (need to load up dataflows entities)
      $html = '<p style="color:red">Error: no category or dataflow selected</p>';
      // $countries = empty($config['dot_stat_dataflows_countries'])?false:explode(',',str_replace(' ','', $config['dot_stat_dataflows_countries']));
      // load dataflows by query
    }

    $libraries = [];
    if ($bootversion = $settings->get('bootstrap')) {
      $libraries[] = 'spc_dot_stat_data/'.$bootversion;
    }
    $libraries[] = 'spc_dot_stat_dataflows/stat-dataflows'; // CSS
    
    // return markup
    return [
      '#markup' => $html,
      '#attached' => [
        'library' => $libraries
      ],
      '#cache' => [
        'max-age' => 3600, //caching chart for 1 hour.
      ]
    ];

  }

  protected function _dataflowLoop($dflows, $constraint, $mode) {
    $html = '';
    switch ($mode) {
    case 'entities':
      $html .= '<ul class="dot-stat-entities">';
      if (is_array($dflows)) {
        foreach($dflows as $dflow) {
          $html .= $this->_dataflowEntity($dflow, $constraint);
        }
      } else {
        $html .= $this->_dataflowEntity($dflows, $constraint);
      }
      $html .= '</ul>';
      break;
    default:
      if ($mode != 'expanded' && $mode != 'expanded_nolimit') {
        $html = '<dl class="dot_stat_dataflows">';
      }
      if (is_array($dflows)) {
        foreach($dflows as $dflow) {
          $html .= $this->_dataflowSingle($dflow, $constraint, $mode);
        }
      } else {
        $html .= $this->_dataflowSingle($dflows, $constraint, $mode);
      }
      if (!$mode != 'expanded' && $mode != 'expanded_nolimit') {
        $html .= '</dl>';
      }
      break;
    }
    return $html;
  }

  protected function _dataflowEntity($flow, $constraint) {

    if (!is_object($flow)) {
      return '<p>Not an object: '.$flow.'</p>';
    }

    $fid = $flow->get('field_code')->value;
    $flowtitle = str_replace(
      [
        'Sustainable Development Goal',
        'National Minimum Development Indicators (NMDI)'
      ], [
        'SDG ',
        'NMDI'
      ],
      $flow->getTitle()
    );

    $flowurl = _df_get_data_explorer_link($flow);
    $nodeurl = Url::fromRoute('entity.node.canonical', ['node' => $flow->id()])->toString();

    $html = '<li class="dot-stat-node">'
      .'<a href="'.$flowurl.'" target="_blank" nofollow class="dot-stat-icon"><span class="dot-stat-label">.stat</span></a>'
      .'<span class="dot-stat-title"><a href="'.$nodeurl.'">'.$flowtitle.'</a></span>';
    
    $desc = $flow->get('field_description')->value;
    if (strlen($desc) > 160) {
      $desc = substr($desc,0,160);
      $isp = strrpos($desc, '.');
      if ($isp === false) {
        $isp = strrpos($desc, ',');
        if ($isp === false) {
          $isp = strrpos($desc, ' ');
        }
      } else {
        $isp += 1;
      }
      $desc = substr($desc,0, $isp).' [...]';
    }
    $html .= '<span class="dot-stat-description">'.nl2br(htmlentities($desc)).'</span>';

    if ($constraint) {

      $subflows = _df_get_sublinks_by_constraint($flowurl, $flow->get('field_constraints')->value, $constraint);

      if ($subflows && count($subflows) > 1) {

        $html .= '<ul class="dot_stat_subflows">';
        foreach ($subflows as $sid => $obj) {
          $html .= '<li>'
            .'<a href="'.$obj->url.'" title="'.htmlentities($obj->description).'" target="_blank">'
            . htmlentities($obj->title)
            .'</a>'
            .'</li>';
        }      
        $html .= '</ul>';

      }

    } else {
        /*
        $subflows = _df_get_sublinks_by_constraint($flowurl, $flow->get('field_constraints')->value, 'INDICATOR');
        if ($subflows && count($subflows) > 1) {
          $html .= '<span class="dot-stat-description">'.count($subflows).' indicators</span>';
        }
        */
    }


    $html .= '</li>';

    return $html;
  }

  protected function _dataflowSingle($flow, $constraint, $mode) {

    $expanded = ($mode == 'expanded' || $mode == 'expanded_nolimit')?true:false;

    if (!is_object($flow)) {
      return '<p>Not an object: '.$flow.'</p>';
    }

    $fid = $flow->get('field_code')->value;
    $flowtitle = str_replace(
      [
        'Sustainable Development Goal',
        'National Minimum Development Indicators (NMDI)'
      ], [
        'SDG ',
        'NMDI'
      ],
      $flow->getTitle()
    );

    $flowurl = _df_get_data_explorer_link($flow);

    $nodeurl = Url::fromRoute('entity.node.canonical', ['node' => $flow->id()])->toString();

    $html = '';
    if ($expanded) {
      $html = '<p class="dot_stat_url_flow">'
        .'<a href="'.$flowurl.'" target="_blank">'.$flowtitle.'</a>'
        .'</p>'
        .'<p class="dot_stat_desc">'.nl2br(htmlentities($flow->get('field_description')->value)).'</p>';
    } else {
      $html = '<dt>';
      $html .= '<i data-toggle="collapse" href="#pdhstat_'.$fid.'" class="pdhstat-toggle collapsed">+</i> ';
      $html .= '<span class="dot-stat-link-wrapper"><a href="'.$nodeurl.'">'.$flowtitle.'</a></span>'
        .'</dt>'
        .'<dd id="pdhstat_'.$fid.'" class="'.($expanded?'':'collapse').'">'
        .'<p>'.nl2br(htmlentities($flow->get('field_description')->value)).'</p>';
    }

    $countflows = 0;

    if ($constraint) {

      $subflows = _df_get_sublinks_by_constraint($flowurl, $flow->get('field_constraints')->value, $constraint);

      if ($subflows && count($subflows) > 1) {

        $maxflows = 999;
        $countflows = count($subflows);
        if (!preg_match('/nolimit$/', $mode)) {
          $maxflows = 10;
        }

        $html .= '<ul class="dot_stat_subflows">';
        foreach ($subflows as $sid => $obj) {
          $maxflows--;
          if ($maxflows < 0) {
            break;
          }
          $html .= '<li>'
            .'<a href="'.$obj->url.'" title="'.htmlentities($obj->description).'" target="_blank">'
            . htmlentities($obj->title)
            .'</a>'
            .'</li>';
        }

        if ($maxflows < 0) {
          $num = $countflows - 10;
          $html .= '<li>... <a href="'.$nodeurl.'">'.$num.' more</a> ...</li>';
        }

        $html .= '</ul>';

      }
    }

    $arrLinks = [];

    if (!$constraint) {
      $lbl = 'List indicators';
      $arrLinks[] = '<a href="'.$flowurl.'" target="_blank" nofollow class="dot-stat-icon"><span class="dot-stat-label">.stat</span></a>'
        .'<a href="'.$nodeurl.'">'.$lbl.'</a>';
    }

    if (!$countflows) {
      $arrLinks[] = '<span class="spc-stat-icon spc-stat-icon-data">&rarr;</span> '
        .'<a href="'.$flowurl.'" target="_blank">'.t('Open dataset').'</a>';
    }

    if ($extr = $flow->get('field_external_resources')->value) {
      $arrext = explode('|', $extr);
      $arrLinks[] = '<span class="spc-stat-icon spc-stat-icon-info">i</span> <a href="'.$arrext[1].'" target="_blank">'.$arrext[0].'</a>';
    }

    if (count($arrLinks) > 0) {
      $html .= '<p class="dot_stat_link">';
      $html .= implode(' &nbsp; ', $arrLinks);
      $html .= '</p>';
    }
    

    $html .= $expanded?'':'</dd>';

    return $html;
  }

}