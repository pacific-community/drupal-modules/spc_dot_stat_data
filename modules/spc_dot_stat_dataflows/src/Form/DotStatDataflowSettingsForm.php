<?php

namespace Drupal\spc_dot_stat_dataflows\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\spc_dot_stat_dataflows\DotStatDataflowSync;
use Drupal\Core\Url;

/**
 * Class DotStatDataflowSettingsForm.
 *
 * @ingroup spc_dot_stat_dataflows
 */
class DotStatDataflowSettingsForm extends ConfigFormBase {

  const SETTINGS = 'spc_dot_stat_dataflows.settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'spc_dot_stat_dataflows_settings';
  }

  /**
   * Defines the settings form for SPC .Stat Data entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    // Default settings.
    $config = $this->config(static::SETTINGS);

    // Sync links
    if (!empty($config->get('dot_stat_nsi'))) {

      $form['dot_stat_sync'] = [
        '#type'   => 'details',
        '#title'  => '.Stat Manual Sync'
      ];

      $form['dot_stat_sync']['intro'] = [
        '#type'  => 'processed_text',
        '#text'  => 'Use buttons below to get category schemes, categories and dataflows from .Stat:'
      ];

      $url = Url::fromRoute('spc_dot_stat_dataflows.sync_categories');
      
      if (true) { // $url->access()
        
        $form['dot_stat_sync']['categories'] = [
          '#type' => 'link',
          '#title' => t('Sync Categories'),
          '#url' => $url,
          '#options' => [
            'attributes' => [
              'class' => [ 'button' ],
              'style' => [ 'margin: 0 1em .5em 0' ]
            ],
          ]
        ];

        $url = Url::fromRoute('spc_dot_stat_dataflows.sync_dataflows');

        $form['dot_stat_sync']['dataflows'] = [
          '#type' => 'link',
          '#title' => t('Sync Dataflows'),
          '#url' => $url,
          '#options' => [
            'attributes' => [
              'class' => [ 'button' ],
              'style' => [ 'margin: 0 1em .5em 0' ]
            ],
          ]
        ];

        /*
        $form['dot_stat_sync']['cleanup'] = [
          '#type' => 'link',
          '#title' => t('Delete Dataflows'),
          '#url' => Url::fromUri('internal:/admin/modules/uninstall/entity/dot_stat_dataflow'),
          '#options' => [
            'attributes' => [
              'class' => [ 'button button--danger' ],
              'style' => [ 'margin: 0 1em .5em 0' ]
            ],
          ]
        ];
        */
      }
    }

    // Force update
    $form['dot_stat_config'] = [
      '#type'   => 'fieldset',
      '#title'  => '.Stat Settings'
    ];

    $form['dot_stat_config']['dot_stat_datasource'] = [ 
      '#type' => 'textfield',
      '#title' => $this->t('DataSource ID'),
      '#size' => 24,
      '#default_value' => $config->get('dot_stat_datasource'),
      '#description' => 'Datasource ID as defined in datasource.json'
    ];

    $form['dot_stat_config']['dot_stat_nsi'] = [ 
      '#type' => 'url',
      '#title' => $this->t('NSI service'),
      '#size' => 48,
      '#default_value' => $config->get('dot_stat_nsi'),
      '#description' => 'URI providing protocol and domain name, e.g.<br /><code>https://stats-nsi-stable.pacificdata.org</code>'
    ];

    $form['dot_stat_config']['dot_stat_explorer'] = [ 
      '#type' => 'url',
      '#title' => $this->t('Data Explorer URI'),
      '#size' => 48,
      '#default_value' => $config->get('dot_stat_explorer'),
      '#description' => 'URI providing protocol and domain name, e.g.<br /><code>https://stats.pacificdata.org</code>'
    ];

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('dot_stat_datasource', $form_state->getValue('dot_stat_datasource'))
      ->set('dot_stat_nsi', $form_state->getValue('dot_stat_nsi'))
      ->set('dot_stat_explorer', $form_state->getValue('dot_stat_explorer'))
      ->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS
    ];
  }

}
