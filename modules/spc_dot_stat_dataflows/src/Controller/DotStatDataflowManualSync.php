<?php

namespace Drupal\spc_dot_stat_dataflows\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\spc_dot_stat_dataflows\DotStatDataflowSync;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Performs a manually triggered sync of digital library content.
 */
class DotStatDataflowManualSync extends ControllerBase {

  /**
   * The .Stat Sync service.
   *
   * @var \Drupal\spc_dot_stat_dataflows\DotStatDataflowSync
   */
  private $syncService;

  /**
   * {@inheritdoc}
   */
  public function __construct(DotStatDataflowSync $dot_stat_sync) {
    $this->syncService = $dot_stat_sync;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('spc_dot_stat_dataflows.sync_service')
    );
  }

  /**
   * Perform a manual sync of both categories and dataflows.
   *
   * @return array
   *   Renderable array with results message.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function syncCategories() {

    /** @var \Drupal\spc_dot_stat_dataflows\DotStatDataflowSync $sync_service */
    // $sync_service = \Drupal::service('spc_dot_stat_dataflows.sync_service');
    $this->syncService->syncCategories();
    return ['#markup' => 'Categories sync completed'];

  }

  /**
   * Perform a manual sync of both categories and dataflows.
   *
   * @return array
   *   Renderable array with results message.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function syncDataflows() {

    /** @var \Drupal\spc_dot_stat_dataflows\DotStatDataflowSync $sync_service */
    // $sync_service = \Drupal::service('spc_dot_stat_dataflows.sync_service');
    $this->syncService->syncDataflows();
    return ['#markup' => 'Dataflows sync completed'];

  }

}
