(function ($, drupalSettings) {
  Drupal.behaviors.entityView = {
    attach: function (context, settings) {
      $('#data-parser').jsonViewer(JSON.parse(drupalSettings['spc_dot_stat_data']['data-parser']));
      $('#data-json').jsonViewer(JSON.parse(drupalSettings['spc_dot_stat_data']['data-json']));
    }
  }
})(jQuery, drupalSettings);