(function ($) {
  Drupal.behaviors.tooltipToggle = {
    attach: function (context, settings) {
      
      if ( $.isFunction($.fn.tooltip) ) {
        $('[data-toggle="tooltip"]', context).tooltip();
      }
        
    }
  }
})(jQuery);